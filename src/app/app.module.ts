import { environment } from './../environments/environment';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IngresosComponent } from './components/tesoreria/ingresos/ingresos.component';
import { EgresosComponent } from './components/tesoreria/egresos/egresos.component';
import { MiembrosComponent } from './components/membresia/miembros/miembros.component';
import { DialogmiembroregComponent } from './components/membresia/dialogmiembroreg/dialogmiembroreg.component';
import { Router, RouterModule } from '@angular/router';
import { LoginComponent } from './components/home/login/login.component';
import { DashboardComponent } from './components/home/dashboard/dashboard.component';
import { HeaderComponent } from './template/header/header.component';
import { MenuComponent } from './template/menu/menu.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { RecaptchaModule, RecaptchaSettings, RECAPTCHA_SETTINGS } from "ng-recaptcha";
import {  MatTableModule } from '@angular/material/table';
import {  MatSidenavModule} from '@angular/material/sidenav';
import {  MatToolbarModule} from '@angular/material/toolbar';
import {  MatIconModule} from '@angular/material/icon';
import {  MatFormFieldModule, MatLabel} from '@angular/material/form-field';
import {MatPaginatorModule} from '@angular/material/paginator';
import {  MatInputModule} from '@angular/material/input';
import { MatExpansionModule} from '@angular/material/expansion';
import {  MatAutocompleteModule} from '@angular/material/autocomplete';
import {  MatDatepickerModule, MatDatepickerToggle} from '@angular/material/datepicker';
import {MatMenuModule} from '@angular/material/menu';
import {MatListModule} from '@angular/material/list';
import {  MatDialogModule,  MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {MatCardModule} from '@angular/material/card';
import {MatSelectModule} from '@angular/material/select';
import {MatChipsModule} from '@angular/material/chips';
import { MatTooltipModule } from '@angular/material/tooltip';
import {MatRadioModule} from '@angular/material/radio';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ButtonComponent } from './template/button/button.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RegistroComponent } from './components/tesoreria/ingresos/registro/registro.component';
import { MatNativeDateModule, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { CajaComponent } from './components/tesoreria/caja/caja.component';
import { TitlemoduleComponent } from './template/titlemodule/titlemodule.component';
import { CategoriasComponent } from './components/configuracion/categorias/categorias.component';
import { DatosiglesiaComponent } from './components/configuracion/datosiglesia/datosiglesia.component';
import { DialogcategoriaregComponent } from './components/configuracion/categorias/dialogcategoriareg/dialogcategoriareg.component';
import { AccesoscajaComponent } from './components/tesoreria/accesoscaja/accesoscaja.component';
import { CajaregComponent } from './components/tesoreria/accesoscaja/cajareg/cajareg.component';
import { CtrlmiembrosComponent } from './components/controles/ctrlmiembros/ctrlmiembros.component';
import { MensajeComponent } from './components/controles/mensaje/mensaje.component';
import { CambiarpassComponent } from './template/cambiarpass/cambiarpass.component';
import { DecimalesDirective } from './directives/decimales.directive';
import { LetrasDirective } from './directives/letras.directive';
import { CtrldialogeliminarComponent } from './components/controles/ctrldialogeliminar/ctrldialogeliminar.component';
import { CtrldialogdetallegrupoComponent } from './components/controles/ctrldialogdetallegrupo/ctrldialogdetallegrupo.component';
import { CtrlsaldoscajaComponent } from './components/controles/ctrlsaldoscaja/ctrlsaldoscaja.component';
import { TransfrubrosComponent } from './components/tesoreria/transfrubros/transfrubros.component';
import { CryptService } from './services/crypt.service';
import { MatCheckboxModule } from '@angular/material/checkbox';
import axios from 'axios';
import { InformeMensualComponent } from './components/tesoreria/informe-mensual/informe-mensual.component';
import { EstadisticasComponent } from './components/tesoreria/estadisticas/estadisticas.component';
import { ConfiguracionInformeComponent } from './components/tesoreria/informe-mensual/configuracion-informe/configuracion-informe.component';
import { CtrlfamiliasComponent } from './components/controles/ctrlfamilias/ctrlfamilias.component';
import { DialogfamiliaregComponent } from './components/membresia/dialogfamiliareg/dialogfamiliareg.component';
import { FamiliasComponent } from './components/membresia/familias/familias.component';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { UsuariosComponent } from './components/seguridad/usuarios/usuarios.component';
import { RegistroUsuarioComponent } from './components/seguridad/usuarios/registro-usuario/registro-usuario.component';
import { PermisosUsuarioComponent } from './components/seguridad/usuarios/permisos-usuario/permisos-usuario.component';
import { AuthInterceptorInterceptor } from './interceptors/auth-interceptor.interceptor';
import { ActivoGuard } from './guards/activo.guard';
// import { CookieService } from 'ngx-cookie-service';
// import { MatMomentDateModule } from "@angular/material-moment-adapter";

@NgModule({
  declarations: [
    // ComponentesPropios
    AppComponent,
    IngresosComponent,
    EgresosComponent,
    MiembrosComponent,
    DialogmiembroregComponent,
    LoginComponent,
    DashboardComponent,
    HeaderComponent,
    MenuComponent,
    ButtonComponent,
    RegistroComponent,
    CajaComponent,
    TitlemoduleComponent,
    CategoriasComponent,
    DatosiglesiaComponent,
    DialogcategoriaregComponent,
    AccesoscajaComponent,
    CajaregComponent,
    CtrlmiembrosComponent,
    MensajeComponent,
    CambiarpassComponent,
    DecimalesDirective,
    LetrasDirective,
    CtrldialogeliminarComponent,
    CtrldialogdetallegrupoComponent,
    CtrlsaldoscajaComponent,
    TransfrubrosComponent,
    InformeMensualComponent,
    EstadisticasComponent,
    ConfiguracionInformeComponent,
    CtrlfamiliasComponent,
    DialogfamiliaregComponent,
    FamiliasComponent,
    UsuariosComponent,
    RegistroUsuarioComponent,
    PermisosUsuarioComponent

    // ComponentesAngular

  ],
  entryComponents: [
    RegistroComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatTableModule,
    MatSidenavModule,
    MatToolbarModule,
    MatIconModule,
    MatDialogModule,
    MatChipsModule,
    NgbModule,
    FormsModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    MatAutocompleteModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatExpansionModule,
    MatListModule,
    MatTooltipModule,
    MatMenuModule,
    MatSelectModule,
    MatCardModule,
    MatProgressSpinnerModule,
    RecaptchaModule,
    MatPaginatorModule,
    HttpClientModule,
    MatChipsModule,
    MatRadioModule,
    ToastrModule.forRoot({
      timeOut: 15000,
      positionClass :'toast-bottom-right'
    })
    //  MatMomentDateModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptorInterceptor, multi: true },
    { provide: MatDialogRef,    useValue: []},
    { provide: MAT_DIALOG_DATA, useValue: [] },
    {
      provide: RECAPTCHA_SETTINGS,
      useValue: { siteKey: environment.SITEKEY } as RecaptchaSettings,
    },
    { provide: MAT_DATE_LOCALE, useValue: 'es-GB' },  { provide: ToastrService }, CryptService, ActivoGuard
    ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
})
export class AppModule { }
