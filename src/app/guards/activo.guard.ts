import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ActivoGuard implements CanActivate {
  constructor(private router: Router) { }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
    console.log("conexion ===> " + this.isLoggedIn())
      if (this.isLoggedIn()) {
        return true;
      }
        // ir a Login
      this.router.navigate(['/login']);
      return false;
  }
  public isLoggedIn(): boolean {
    let $state: boolean = true;
    if (sessionStorage.getItem("token") !== '' && sessionStorage.getItem("token") !== null) {
      if (sessionStorage.getItem("user") !== '' && sessionStorage.getItem("user") !== null) {
        $state = true;
      }else{
        $state = false;
      }
    }else{
      $state = false;
    }

    return $state;
  }

}

