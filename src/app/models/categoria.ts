export interface Categoria {
  row: number,
  id: string;
  codigo: string;
  abreviatura: string;
  descripcion: string;
  activo: any;
  superior: string;
  fecharegistro: Date;
  usuarioregistro: string;
  fechaactualiza: Date;
  usuarioactualiza: string;
  descsuperior: string;
  idcategoria: string;
  descategoria: string;
  tipo: number
}

export interface CategoriaCbo {
  id: string;
  superior: string;
  descripcion: string;
}


