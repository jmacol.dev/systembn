export interface transferenciarubros{
  row: number;
  id: string;
  fecha: Date;
  monto: any;
  activo: string;
  idrubrocargo: string;
  idrubroabona: string;
  idcaja: string;
  fecharegistro: Date;
  usuarioregistro: string;
  fechaactualiza: Date;
  usuarioactualiza: string;
  rubrocargo: string;
  rubroabona: string;
  caja: string;
}
