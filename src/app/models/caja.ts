export interface Caja {
  row: number;
  id: string;
  codigo: string;
  nombre: string;
  aperturaInicial: any;
  saldo: any;
  activo: number;
  responsable: string;
  fecharegistro: Date;
  usuarioregistro: string;
  fechaactualiza: Date;
  usuarioactualiza: string;
  aperturadiezmos : any;
  aperturaofrendas : any;
  aperturamisiones : any;
  aperturamisericordia : any;
  aperturaprotemplo : any;
  saldodiezmos : any;
  saldoofrendas : any;
  saldomisiones : any;
  saldomisericordia : any;
  saldoprotemplo : any;
  nombreresponsable: string;
}

export interface CajaCbo {
  row: number,
  id: string,
  nombre: string,
  codigo: string
}

export interface CajaSaldo {
  id: string,
  saldo: any,
  diezmos: any,
  ofrendas: any,
  misiones: any,
  misericordia: any,
  protemplo: any
}
