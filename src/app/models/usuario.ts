export interface Usuario {
  id: number;
  nombre: string;
  apellidoPaterno: string;
  apellidoMaterno: string;
  nombreCompleto: string;
  usuario: string;
  password: string;
  iglesia: string;
  perfil: string;
}


export interface usuarioTable {
  id: string;
  nombreCompleto: string;
  telefonos: string;
  email: string;
  usuario: string;
  administrador: any;
  activo: any;
  confirmado: any;
  fecharegistro: Date;
}
