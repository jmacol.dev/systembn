export interface Familia {
  id: string,
  apellidopaterno: string,
  apellidomaterno: string,
  activo: number,
  fecharegistro: Date,
  usuarioregistro: string,
  fechaactualiza: Date,
  usuarioactualiza: string
}


export interface FamiliaCbo {
  row: number;
  id: string;
  nombreCompleto: string;
}
