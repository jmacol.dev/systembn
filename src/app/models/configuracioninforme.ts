export interface configuracioninforme {
  id: string;
  codigo: string;
  descripcion: string,
  tipo: any;
  detalle01: any;
  detalle02: any;
  activo: any;
  fecharegistro: Date;
  usuarioregistro: string;
  fechaactualiza: Date;
  usuarioactualiza: string;
}
