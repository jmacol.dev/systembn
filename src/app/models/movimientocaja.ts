export interface movimientocaja{
  row: number;
  id: string;
  fecha: Date;
  indicador: number;
  comentario: string;
  activo: number;
  idCaja: string;
  idtipomovimiento: string;
  idgruporecaudacion: string;
  responsable: string;
  aportefamiliar: string,
  autorizacion: string;
  fecharegistro: Date;
  usuarioregistro: string;
  fechaactualiza: Date;
  usuarioactualiza: string;
  monto: any;
  idtipogasto: string;
  motivoeliminacion: string;
  fechaeliminacion: Date;
  usuarioeliminacion: string;
  destino: string;
  nombreresponsable: string;
  nombreautoriza: string;
  caja: string;
  gruporecaudacion: string;
  tipomovimiento: string;
  detallemovimiento: string;
}



