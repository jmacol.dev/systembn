export interface gruporecaudacion{
  row: number;
  id: string;
  codigo: string;
  fecha: Date;
  fechacierre: Date;
  activo: string;
  primertestigo: string;
  segundotestigo: string;
  fecharegistro: Date;
  usuarioregistro: string;
  fechaactualiza: Date;
  usuarioactualiza: string;
  idcaja: string;
  monto: any,
  caja: string;
  nombreprimertestigo: string;
  nombresegundotestigo: string;
  nombreusuario: string;

}
