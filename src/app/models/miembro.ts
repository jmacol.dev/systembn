export interface Miembro {
  row: number;
  id: string,
  nombres: string,
  apellidopaterno: string,
  apellidomaterno: string,
  fechanacimiento: Date,
  email: string,
  telefono1: string,
  telefono2: string,
  sexo: number,
  clasificacionedad: string,
  familia: string,
  miembro: Boolean,
  activo: number,
  fecharegistro: Date,
  usuarioregistro: string,
  fechaactualiza: Date,
  usuarioactualiza: string,
  autorizaegreso: number,
}

export interface MiembroCbo {
  row: number;
  id: string;
  nombreCompleto: string;
}

export interface MiembroBusq {
  sexo: any;
  clasificacionedad: string;
  tipo: any;
}


export interface MiembroTable {
  id: string,
  nombres: string,
  apellidopaterno: string,
  apellidomaterno: string,
  fechanacimiento: Date,
  email: string,
  telefono1: string,
  telefono2: string,
  sexo: number,
  clasificacionedad: string,
  familia: string,
  miembro: number,
  activo: number

}
