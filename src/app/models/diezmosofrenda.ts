export interface diezmoofrenda {
  row: number;
  id: string;
  diezmo: any;
  ofrenda: any;
  misiones: any;
  misericordia: any;
  protemplo: any;
  total: any;
  activo: number;
  idgruporecaudacion: string;
  idaportante: string;
  aportefamiliar: string,
  fecharegistro: Date;
  usuarioregistro: string;
  fechaactualiza: Date;
  usuarioactualiza: string;
  motivoeliminacion: string;
  fechaeliminacion: Date;
  usuarioeliminacion: string;
  aportante: string;
  caja: string;
  gruporecaudacion: string;

}
