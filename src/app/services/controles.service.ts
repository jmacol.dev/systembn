import { BreakpointObserver } from '@angular/cdk/layout';
import { EventEmitter, Injectable, Output } from '@angular/core';
import { MatDrawerMode } from '@angular/material/sidenav';
import { CajaSaldo } from '../models/caja';
import { gruporecaudacion } from '../models/gruporecaudacion';
import { ConfiguracionService } from './configuracion.service';
import { ListService } from './list.service';
import { RegisterService } from './register.service';

@Injectable({
  providedIn: 'root'
})

export class ControlesService {
  @Output() openList = new EventEmitter();
  public persona_aut!: string;
  public persona_ent!: string;
  public persona: any
  public disctrolmbr: boolean = true
  public diasmax: number = 2
  public minfechadias: number = 2
  public saldorubro: any = 0.0.toFixed(2)
  public saldorubrotransf: any = 0.0.toFixed(2)
  nmbrctrl: string= ''
  public gruporecaudacion_lst!: gruporecaudacion[];
  public grecaud_aux!: gruporecaudacion[];
  public cajasaldo: CajaSaldo = this.regserv.empty_cajasaldo();
  public vsald: boolean = false
  public isSmallScreen: any
  public modesidenav: MatDrawerMode = "push" as MatDrawerMode
  constructor(
    public regserv: RegisterService,
    public listserv: ListService,
    public configserv: ConfiguracionService,
    public breakpointObserver: BreakpointObserver
  ) {
    this.isSmallScreen = breakpointObserver.isMatched('(max-width: 970px)');
  }

  cargardatos($tipo: boolean, $id: string){
    // console.log($datos)

  }

  getcajasaldo(){
    this.listserv.listado('get-cajasaldo', '?idcaja='+ this.regserv.caja_reg.id).then(data=>{
      this.cajasaldo = data;
    });
  }

  seleccionarcaja(){
    this.gruporecaudacion_lst = []
    this.getcajasaldo();
    this.gruporecaudacion_lst = this.grecaud_aux.filter(item => item.idcaja == (this.regserv.caja_reg.id == 'TODOS' ? item.idcaja : this.regserv.caja_reg.id)&& (item.activo == '\u0001' || item.activo == '1')
      && item.fecha.toString().substr(0, 10) == this.configserv.datepipe.transform(this.regserv.movimientocaja_reg.fecha, "YYYY-MM-dd"))
    this.regserv.gruporecaudacion_reg = this.gruporecaudacion_lst.length > 0 ? this.gruporecaudacion_lst[0] : this.regserv.empty_gruporecaudacion()

      this.regserv.gruporecaudacion_reg.primertestigo = ''
      this.regserv.gruporecaudacion_reg.segundotestigo = ''
      this.listserv.listar('listardo')
  }
  listargruporecaudacion($opcion: number){
    this.listserv.listado('list-gruporecaudacion', '?idcaja=TODOS&tipo=0').then(data=>{
      this.grecaud_aux = data;
      if(this.regserv.caja_reg.id == 'TODOS'){
        let gr_rec: gruporecaudacion[] = data
        this.gruporecaudacion_lst = gr_rec.filter(item => item.fecha.toString().substr(0, 10) == this.configserv.datepipe.transform(new Date(), "YYYY-MM-dd")?.toString())

      }else{
        this.seleccionarcaja()
      }
      this.regserv.gruporecaudacion_reg = this.gruporecaudacion_lst.length > 0 ? this.gruporecaudacion_lst[0] : this.regserv.empty_gruporecaudacion()
      if ($opcion === 2){
        this.listserv.listar(this.openList.emit(null))
      }
    })
  }

}
