import { EventEmitter, Injectable, Output } from '@angular/core';
import axios from "axios";
import { environment } from "./../../environments/environment";
import { HeaderautorizaService } from './headerautoriza.service';
import { CategoriaCbo } from '../models/categoria';
import { PaginationService } from './pagination.service';
import { PageEvent } from '@angular/material/paginator';
import { diezmoofrenda } from '../models/diezmosofrenda';
import { SeguridadService } from './seguridad.service';
import { SesionService } from './sesion.service';
@Injectable({
  providedIn: 'root'
})
export class ListService {

  pageEvent: PageEvent = new PageEvent;
  public fechamax!: Date;
  public fechamin!: Date;
  public opcionreporte: number = 0;
  constructor(
    public hautserv: HeaderautorizaService,
    public pageService: PaginationService,
    public sserv: SesionService
  ) { }

  public listar(listar: any){
    return listar;
  }

  public excel(excel: any){
    return excel;
  }

  public pdf(pdf: any){
    return pdf;
  }

  public listadodestino = async()=>{
    let $data: CategoriaCbo[] = []
      this.listado('list-rubroscaja', '').then(data =>{
        $data = data;
      })
      return $data;
  }

  public listado = async  ($route: string, $request: any)  => {
    let $data: any = [];
    if($request.trim() == ''){
      $request += '?token='+ this.sserv.gettoken.toString()
    }else{
      $request += '&token='+ this.sserv.gettoken.toString()
    }
    await axios.get(environment.URLAPI + $route + $request)
      .then((response) => {
        if (response.data.estado) {
          $data = response.data.data;
        }
      })
      .catch( (error) =>{
        console.log(error);
      });
      return $data;
  }


  getTotal($tipo: number, $dataGlobal: diezmoofrenda[]){
    let $total: any = 0;
    switch ($tipo){
      case 1:
        $total = $dataGlobal
          .reduce(
            (sum, value) =>
              typeof parseFloat(value.diezmo) === 'number'
                ? sum + parseFloat(value.diezmo)
                : sum, 0
          )
          .toFixed(2);
        break;
      case 2:
        $total = $dataGlobal
          .reduce(
            (sum, value) =>
              typeof parseFloat(value.ofrenda) === 'number'
                ? sum + parseFloat(value.ofrenda)
                : sum, 0
          )
          .toFixed(2);
        break;
      case 3:
        $total = $dataGlobal
          .reduce(
            (sum, value) =>
              typeof parseFloat(value.misiones) === 'number'
                ? sum + parseFloat(value.misiones)
                : sum, 0
          )
          .toFixed(2);
        break;
      case 4:
        $total = $dataGlobal
          .reduce(
            (sum, value) =>
              typeof parseFloat(value.misericordia) === 'number'
                ? sum + parseFloat(value.misericordia)
                : sum, 0
          )
          .toFixed(2);
        break;
      case 5:
        $total = $dataGlobal
          .reduce(
            (sum, value) =>
              typeof parseFloat(value.protemplo) === 'number'
                ? sum + parseFloat(value.protemplo)
                : sum, 0
          )
          .toFixed(2);
        break;
      default :
        $total = $dataGlobal
          .reduce(
            (sum, value) =>
              typeof parseFloat(value.total) === 'number'
                ? sum + parseFloat(value.total)
                : sum, 0
          )
          .toFixed(2);
        break;
    }
    return $total;
  }
}
