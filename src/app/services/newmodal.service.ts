import { PermisosUsuarioComponent } from './../components/seguridad/usuarios/permisos-usuario/permisos-usuario.component';
import { RegistroUsuarioComponent } from './../components/seguridad/usuarios/registro-usuario/registro-usuario.component';
import { NotificacionesService } from './notificaciones.service';
import { DialogfamiliaregComponent } from './../components/membresia/dialogfamiliareg/dialogfamiliareg.component';
import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CtrldialogdetallegrupoComponent } from '../components/controles/ctrldialogdetallegrupo/ctrldialogdetallegrupo.component';
import { CtrldialogeliminarComponent } from '../components/controles/ctrldialogeliminar/ctrldialogeliminar.component';
import { DialogmiembroregComponent } from '../components/membresia/dialogmiembroreg/dialogmiembroreg.component';
import { TransfrubrosComponent } from '../components/tesoreria/transfrubros/transfrubros.component';
import { ConfiguracionService } from './configuracion.service';
import { ValidateService } from './validate.service';

@Injectable({
  providedIn: 'root'
})
export class NewmodalService {

  constructor(
    public dialog: MatDialog,
    public configserv: ConfiguracionService,
    public valserv: ValidateService
  ) { }
  nuevoMiembro($id: string, $listar:boolean){
    const dialogRef = this.dialog.open(DialogmiembroregComponent, {
      width: '700px',
      panelClass: 'divmodal',
      data: { title: 'Registro de personas', id: $id, listar: $listar },
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(() => {
      console.log('The dialog was closed');
    });
  }

  nuevoUsuario($id: string, $listar:boolean){
    const dialogRef = this.dialog.open(RegistroUsuarioComponent, {
      width: '550px',
      panelClass: 'divmodal',
      data: { title: 'Registro de usuarios', id: $id, listar: $listar },
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(() => {
      console.log('The dialog was closed');
    });
  }

  permisosUsuario($id: string, $listar:boolean){
    const dialogRef = this.dialog.open(PermisosUsuarioComponent, {
      width: '500px',
      panelClass: 'divmodal',
      data: { title: 'Permisos de usuarios', id: $id, listar: $listar },
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(() => {
      console.log('The dialog was closed');
    });
  }


  nuevaFamilia($id: string, $listar: boolean){
    const dialogRef = this.dialog.open(DialogfamiliaregComponent, {
      width: '500px',
      panelClass: 'divmodal',
      data: {title: 'Registro de familias', id: $id, listar: $listar},
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(() => {
      console.log('The dialog was closed');
    });
  }

  nuevoModalEliminar($title: string, $descripcion: string, $id: string, $route:string){
    const dialogRef = this.dialog.open(CtrldialogeliminarComponent, {
      width: '50%',
      panelClass: 'divmodal',
      data: {title: $title, descripcion: $descripcion, id: $id, route: $route},
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(() => {
      console.log('The dialog was closed');
    });
  }

  nuevatransferenciarubros($value: string){
    if($value.trim() == ''){
      this.valserv.validaban = false;
      this.valserv.showError('Seleccione rubro de destino abonar transferencia.', 'Tesorería - Transferencias')
    }else{
      const dialogRef = this.dialog.open(TransfrubrosComponent, {
        width: '350px',
        panelClass: 'divmodal',
        data: {title: 'Transferencias', abono: $value},
        disableClose: true
      });
      dialogRef.afterClosed().subscribe(() => {
        console.log('The dialog was closed');
      });
    }
  }

  nuevoModalDetalleGrupo($title: string, $grupo: string){
    const dialogRef = this.dialog.open(CtrldialogdetallegrupoComponent, {
      width: '90%',
      panelClass: 'divmodal',
      data: {title: $title,  grupo: $grupo},
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(() => {
      console.log('The dialog was closed');
    });
  }
}
