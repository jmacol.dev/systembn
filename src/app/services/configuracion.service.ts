import { DatePipe } from '@angular/common';
import { Injectable } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { isTemplateExpression } from 'typescript';
import { Caja } from '../models/caja';
import { Categoria } from '../models/categoria';
import { ListService } from './list.service';
import { PaginationService } from './pagination.service';
const ELEMENT_DATA: Categoria[] = []

const ELEMENT_DATA2: Categoria[] = [];
@Injectable({
  providedIn: 'root'
})
export class ConfiguracionService {
  public iconcat: string = 'arrow-circle-right';
  public textcat: string = 'Ir a subcategorías';
  public stcat: boolean = false;
  public btncat: string = 'dark';
  public idcateg: string = '';
  public idsuperiorsub: string = '';
  public tipocat: string = 'categorías';
  public datacategoria: Categoria[] = [];
  public datacategoriaglobal: Categoria[] = [];
  public datasubcategoriaglobal: Categoria[] = [];
  public combocategoria: Categoria[] = [];
  public combosubcategoria: Categoria[] = [];
  public maxlength: number = 50
  public maxlengthcom: number = 200
  public datepipe: DatePipe = new DatePipe('en-US')
  public datoscaja: Caja[] = []
  constructor(
    private pageserv: PaginationService,
    private listserv: ListService

  ) {
    this.datacategoria = ELEMENT_DATA;

  }

  seleccioncategoria(){
    this.datacategoria = []
    this.datacategoriaglobal  = this.datacategoria;
  }


  cargar_datacombo(data: Categoria[], $categoria: string, $subcategoria: string){
    if (!this.stcat){
      this.combocategoria = data;
    }else{
      if($categoria != ''){
        this.combosubcategoria = data.filter(item => item.idcategoria === $categoria)
      }
    }
    let $datareg: Categoria[] = data;
    if(this.idcateg == ''){
      this.datacategoria = data;
      this.datacategoriaglobal = data;
    }else{
      if (!this.stcat){
        this.datacategoria = $datareg.filter(item => item.superior === $categoria)
      }else{
        if($subcategoria.trim() == ''){
          this.datacategoria = $datareg.filter(item => item.idcategoria === $categoria)
        }else{
          this.datacategoria = $datareg.filter(item => item.idcategoria === $categoria  && item.superior === $subcategoria)
        }

      }

    }
    this.datacategoriaglobal = this.datacategoria
    let pageEvent: PageEvent = new PageEvent;
    pageEvent.length = data.length
    pageEvent.pageIndex = 0
    pageEvent.pageSize = 5
    let datalst = this.pageserv.actualizaTabla2(this.datacategoriaglobal, pageEvent)
    this.datacategoria = datalst
  }

  listado_categoria($categoria: string, $subcategoria: string){
    let $route: string = 'list-categoria';
    let $request = '?activo=-1';
    if (this.stcat){
      $route = 'list-subcategoria';
    }
      this.listserv.listado($route, $request).then(data =>{
        if(data.length != this.datacategoriaglobal.length){
        this.cargar_datacombo(data, $categoria, $subcategoria)
        }
      })

  }

  onNoClickDialog($dialogRef: any): void {
    $dialogRef.close();
  }
}
