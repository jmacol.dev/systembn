import { usuarioTable } from './../models/usuario';
import { configuracioninforme } from './../models/configuracioninforme';
import { NotificacionesService } from './notificaciones.service';
import { MiembroBusq } from './../models/miembro';
import { Familia } from './../models/familia';
import { Injectable } from '@angular/core';
import { ValidateService } from './validate.service';
import Swal from 'sweetalert2';
import axios from "axios";
import { environment } from "./../../environments/environment";
import { HeaderautorizaService } from './headerautoriza.service';
import { ListService } from './list.service';
import { Categoria } from '../models/categoria';
import { ConfiguracionService } from './configuracion.service';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Miembro } from '../models/miembro';
import { movimientocaja } from '../models/movimientocaja';
import { diezmoofrenda } from '../models/diezmosofrenda';
import { Caja, CajaSaldo } from '../models/caja';
import { gruporecaudacion } from '../models/gruporecaudacion';
import { Usuario } from '../models/usuario';
import { transferenciarubros } from '../models/transferencias';
import { SeguridadService } from './seguridad.service';
import { SesionService } from './sesion.service';
import { title } from 'process';
@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  public categoria_reg: Categoria = this.empty_categoria()
  public subcategoria_reg: Categoria = this.empty_categoria()
  public miembro_reg: Miembro = this.empty_miembro()
  public familia_reg: Familia = this.empty_familia()
  public movimientocaja_reg: movimientocaja = this.empty_movimientocaja()
  public diezmoofrenda_reg: diezmoofrenda  =  this.empty_diezmoofrenda()
  public caja_reg: Caja  =  this.empty_caja()
  public gruporecaudacion_reg: gruporecaudacion = this.empty_gruporecaudacion()
  public transferenciarubro_reg: transferenciarubros = this.empty_transferenciarubro()
  public miembro_busq: MiembroBusq = this.empty_miembrobusq()
  public configuracioninforme_reg: configuracioninforme = this.empty_configuracioninforme()
  empty_configuracioninforme(): configuracioninforme{
    return {
      id: '',
      codigo: '',
      descripcion: '',
      tipo: '0',
      detalle01: '0',
      detalle02: '0',
      activo: '1',
      fecharegistro: new Date(),
      usuarioregistro: '',
      fechaactualiza: new Date(),
      usuarioactualiza: '',
    }
  }
  empty_transferenciarubro(): transferenciarubros{
    return {row: 0,
      id: '',
      fecha: new Date(),
      monto: 0.0.toFixed(2),
      activo: '1',
      idrubrocargo: '',
      idrubroabona: '',
      idcaja: '',
      fecharegistro: new Date(),
      usuarioregistro: '',
      fechaactualiza: new Date(),
      usuarioactualiza: '',
      rubrocargo: '',
      rubroabona: '',
      caja: ''
    }
  }
  empty_gruporecaudacion(): gruporecaudacion {
    return {
      row: 0,
      id: 'TODOS',
      codigo: '',
      fecha:  new Date(),
      fechacierre: new Date(),
      activo: '',
      primertestigo: '',
      segundotestigo: '',
      fecharegistro:  new Date(),
      usuarioregistro: '',
      fechaactualiza: new Date(),
      usuarioactualiza: '',
      idcaja: '',
      monto: 0.0.toFixed(2),
      caja: '',
      nombreprimertestigo: '',
      nombresegundotestigo: '',
      nombreusuario: ''
    }
  }
  empty_categoria(): Categoria {
    return {
      row: 0,
      id: '',
      codigo: '',
      abreviatura: '',
      descripcion: '',
      activo: 1,
      superior: '',
      fecharegistro: new Date(),
      usuarioregistro: '',
      fechaactualiza: new Date(),
      usuarioactualiza: '',
      descsuperior: '',
      idcategoria: '',
      descategoria: '',
      tipo: 0
    }
  }

  empty_miembro(): Miembro {
    return {
      row: 0,
      id: '',
      nombres: '',
      apellidopaterno: '',
      apellidomaterno: '',
      fechanacimiento: new Date(),
      email: '',
      telefono1: '',
      telefono2: '',
      sexo: -1,
      clasificacionedad: '',
      familia:'',
      miembro: false,
      activo: 1,
      fecharegistro: new Date(),
      usuarioregistro: '',
      fechaactualiza: new Date(),
      usuarioactualiza: '',
      autorizaegreso: 0
    }
  }

  empty_familia(): Familia {
    return {
      id: '',
      apellidopaterno: '',
      apellidomaterno: '',
      activo: 1,
      fecharegistro: new Date(),
      usuarioregistro: '',
      fechaactualiza: new Date(),
      usuarioactualiza: '',
    }
  }
empty_miembrobusq(): MiembroBusq {
    return {
      sexo: '-1',
      clasificacionedad: '0',
      tipo: '-1'
    }
  }

  empty_movimientocaja(): movimientocaja {
    return {
      row: 0,
      id: '',
      fecha: new Date(),
      indicador: 0,
      comentario: '',
      activo: 1,
      idCaja: '',
      idtipomovimiento: '',
      idgruporecaudacion: '',
      responsable: '',
      aportefamiliar: '',
      autorizacion: '',
      fecharegistro: new Date(),
      usuarioregistro: '',
      fechaactualiza: new Date(),
      usuarioactualiza: '',
      monto: 0.0.toFixed(2),
      idtipogasto: '',
      motivoeliminacion: '',
      fechaeliminacion: new Date(),
      usuarioeliminacion: '',
      destino: '',
      nombreresponsable: '',
      nombreautoriza: '',
      caja: '',
      gruporecaudacion: '',
      tipomovimiento: '',
      detallemovimiento: ''
    }
  }

  empty_diezmoofrenda(): diezmoofrenda {
    return {
      row: 0,
      id: '',
      diezmo: 0.0.toFixed(2),
      ofrenda: 0.0.toFixed(2),
      misiones: 0.0.toFixed(2),
      misericordia: 0.0.toFixed(2),
      protemplo: 0.0.toFixed(2),
      total: 0.0.toFixed(2),
      activo: 1,
      idgruporecaudacion: '',
      idaportante: '',
      aportefamiliar: '',
      fecharegistro: new Date(),
      usuarioregistro: '',
      fechaactualiza: new Date(),
      usuarioactualiza: '',
      motivoeliminacion: '',
      fechaeliminacion: new Date(),
      usuarioeliminacion: '',
      aportante: '',
      caja: '',
      gruporecaudacion: ''
    }
  }

  empty_usuario(): Usuario{
    return {
      id: 0,
      nombre: '',
      apellidoPaterno: '',
      apellidoMaterno: '',
      nombreCompleto: 'Nombre de usuario',
      usuario: '',
      password: '',
      iglesia: '',
      perfil: 'Administrador'
     }
  }

  empty_cajasaldo(): CajaSaldo{
    return {
      id: '',
      saldo: 0.0.toFixed(2),
      diezmos: 0.0.toFixed(2),
      ofrendas:0.0.toFixed(2),
      misiones:0.0.toFixed(2),
      misericordia:0.0.toFixed(2),
      protemplo:0.0.toFixed(2),
    }
  }

  empty_usuarioTable(): usuarioTable{
    return {
      id: '',
      nombreCompleto: '',
      telefonos: '',
      email: '',
      usuario: '',
      administrador: 0,
      activo: 0,
      confirmado: 0,
      fecharegistro: new Date(),
    }
  }

  empty_caja(): Caja{
    return {
      row: 0,
      id: 'TODOS',
      codigo: '',
      nombre: '',
      aperturaInicial: 0.0.toFixed(2),
      saldo: 0.0.toFixed(2),
      activo: 1,
      responsable: '',
      fecharegistro: new Date(),
      usuarioregistro: '',
      fechaactualiza: new Date(),
      usuarioactualiza: '',
      aperturadiezmos : 0.0.toFixed(2),
      aperturaofrendas : 0.0.toFixed(2),
      aperturamisiones : 0.0.toFixed(2),
      aperturamisericordia : 0.0.toFixed(2),
      aperturaprotemplo : 0.0.toFixed(2),
      saldodiezmos : 0.0.toFixed(2),
      saldoofrendas : 0.0.toFixed(2),
      saldomisiones : 0.0.toFixed(2),
      saldomisericordia : 0.0.toFixed(2),
      saldoprotemplo : 0.0.toFixed(2),
      nombreresponsable:  '',
    }
  }

  constructor(public valserv: ValidateService,
    public hautserv: HeaderautorizaService,
    public listserv: ListService,
    public configservc: ConfiguracionService,
    public sserv: SesionService) { }

  public registro = async ($title: string, $route: string, $request: any) => {
    console.log(JSON.stringify($request))
    let $dataret: boolean = true;
      $request.token = this.sserv.gettoken.toString()
      await axios.post(environment.URLAPI + $route, $request, {
      headers: this.hautserv.headers_aut,
      })
        .then((response) => {
        console.log(JSON.stringify(response))
        if (response.data.estado) {
          $dataret = true;
          Swal.fire($title, response.data.mensaje, response.data.codigo == 1 ? "success" : "info");
        }else{
          this.valserv.validaban = false
          this.valserv.showError(response.data.mensaje, title)
          $dataret = false;
        }
      })
      .catch( (error) =>{
        $dataret = false;
        this.valserv.validaban = false
          this.valserv.showError(error, title)
        // console.log(error);
      });
    return $dataret;
  }

  public actualizar = async ($title: string, $route: string, $request: any) => {

    let $dataret: boolean = true;
    $request.token = this.sserv.gettoken.toString()
      await axios.put(environment.URLAPI + $route, $request, {
      headers: this.hautserv.headers,
      })
      .then((response) => {
        if (response.data.estado) {
          $dataret = true;
          Swal.fire($title, response.data.mensaje, response.data.codigo == 1 ? "success" : "info");
        }else{
          this.valserv.validaban = false
          this.valserv.showError(response.data.mensaje, title)
          $dataret = false;
        }
      })
      .catch( (error) =>{
        $dataret = false;
        this.valserv.validaban = false
        this.valserv.showError(error, title)
        console.log(error);
      });
    return $dataret;
  }
}
