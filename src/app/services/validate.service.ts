import { ToastrService } from 'ngx-toastr';
import { SeguridadService } from './seguridad.service';
import { NotificacionesService } from './notificaciones.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ValidateService {
  public validaban: boolean = true;
  // public validatext: string = '';
  // public validatype: string = 'danger';
  constructor(private toastr: ToastrService) { }

  showError(msj: string, title: string) {
    this.toastr.error(msj, title);
  }

  showSuccess(msj: string, title: string) {
    this.toastr.success(msj, title);
  }

  showInfo(msj: string, title: string) {
    this.toastr.info(msj, title, {timeOut: 30000});
  }
  validatelogin($usuario: string, $password: string): boolean{
    this.validaban = true;
    if($usuario.trim() == '' && $password.trim() == ''){
      this.validaban = false;
      this.showError('Ingrese sus credencales', "Inicio de Sesión")
    }else if($usuario.trim() == ''){
      this.validaban = false;
      this.showError('Ingrese su usuario', "Inicio de Sesión")
    }else if($password.trim() == ''){
      this.validaban = false;
      this.showError('Ingrese su contraseña', "Inicio de Sesión")
    }
    return this.validaban;
  }

  validatecategoria($descripcion: string, modulo: string, title:string): boolean{
    this.validaban = true;
    if($descripcion.trim() == ''){
      this.validaban = false;
      this.showError('Ingrese descripción de categoría.', modulo + ' - ' + title)
    }
    return this.validaban;
  }

  validatediezmoofrenda($monto: any, modulo: string, title:string): boolean{
    this.validaban = true;
    if(parseFloat($monto).toFixed(2) == 0.0.toFixed(2)){
      this.validaban = false;
      this.showError('El monto aportado debe ser mayor a S/ 0.00', modulo + ' - ' + title)
    }
    return this.validaban;
  }

  validarcierregrupo($monto: any, $primertestigo:string, $segundotestigo:string, $caja: string, modulo: string, title:string): boolean{
    this.validaban = true;
    if($caja == 'TODOS'){
      this.validaban = false;
      this.showError('Seleccione la caja que corresponde al grupo de recaudación.', modulo + ' - ' + title)
    }else if(parseFloat($monto).toFixed(2) == 0.0.toFixed(2)){
      this.validaban = false;
      this.showError('El monto del grupo de recaudación debe ser mayor a S/ 0.00, agregue diezmos y ofrendas.', modulo + ' - ' + title)
    }else if($primertestigo == '' && $segundotestigo == ''){
      this.validaban = false;
      this.showError('Debe seleccionar al menos un testigo.', modulo + ' - ' + title)
    }else if($primertestigo == $segundotestigo){
      this.validaban = false;
      this.showError('Los testigos no deben ser la misma persona.', modulo + ' - ' + title)
    }else if($primertestigo == '' && $segundotestigo != ''){
      this.validaban = false;
      this.showError('El segundo testigo bórrelo y seleccione como primer testigo.', modulo + ' - ' + title)
    }
    return this.validaban;
  }



  validatesubcategoria($descripcion: string, $categoria: string, modulo: string, title:string): boolean{
    this.validaban = true;
    if($categoria.trim() == ''){
      this.validaban = false;
      this.showError('Seleccione categoría.', modulo + ' - ' + title)
    }else if($descripcion.trim() == ''){
      this.validaban = false;
      this.showError('Ingrese descripción de subcategoría.', modulo + ' - ' + title)
    }
    return this.validaban;
  }
}
