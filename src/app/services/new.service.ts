import { diezmoofrenda } from './../models/diezmosofrenda';
import { ConfiguracionInformeComponent } from './../components/tesoreria/informe-mensual/configuracion-informe/configuracion-informe.component';
import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DialogcategoriaregComponent } from '../components/configuracion/categorias/dialogcategoriareg/dialogcategoriareg.component';
import { CajaregComponent } from '../components/tesoreria/accesoscaja/cajareg/cajareg.component';
import { EgresosComponent } from '../components/tesoreria/egresos/egresos.component';
import { RegistroComponent } from '../components/tesoreria/ingresos/registro/registro.component';
import { ConfiguracionService } from './configuracion.service';
import { RegisterService } from './register.service';

@Injectable({
  providedIn: 'root'
})
export class NewService {
public _egreso: boolean = true
public nvoval: string = '';
  constructor(
    public dialog: MatDialog,
    public configserv: ConfiguracionService,
    private regserv: RegisterService
  ) { }

  public nuevo(nuevo: any){
    return nuevo;
  }

  nuevaCategoria($state: boolean, $categ: any){
    if(!$state){
      this.regserv.categoria_reg.descripcion = ''
      this.regserv.categoria_reg.superior = this.configserv.idsuperiorsub
      this.regserv.categoria_reg.idcategoria = this.configserv.idcateg
      this.regserv.categoria_reg.id = ''
    }else{
      this.regserv.categoria_reg = $categ
      if (!this.configserv.stcat){
        this.configserv.listado_categoria(this.regserv.categoria_reg.superior, '')
        this.regserv.categoria_reg.idcategoria = $categ.superior
      }else{
        this.configserv.listado_categoria(this.regserv.categoria_reg.idcategoria, this.regserv.categoria_reg.superior ?? '')
        this.regserv.categoria_reg.superior = $categ.superior ?? ''
      }

    }
    const dialogRef = this.dialog.open(DialogcategoriaregComponent, {
      width: '350px',
      panelClass: 'divmodal',
      data: {title: 'Registro de ' + this.configserv.tipocat},
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(() => {
      console.log('The dialog was closed');
    });
  }

  nuevoingreso($id: string, $data: diezmoofrenda, $listar: boolean){
    const dialogRef = this.dialog.open(RegistroComponent, {
      width: '50%',
      panelClass: 'divmodal',
      data: {title: 'Registro de ingresos', id: $id, data: $data, listar: $listar},
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(() => {
      console.log('The dialog was closed');
    });
  }



  nuevoegreso(){
    if(this._egreso){
      const dialogRef = this.dialog.open(EgresosComponent, {
        width: '50%',
        panelClass: 'divmodal',
        data: {title: 'Registro de egresos', egreso: this._egreso},
        disableClose: true
      });
      dialogRef.afterClosed().subscribe(() => {
        console.log('The dialog was closed');
      });
    }else{
      const dialogRef = this.dialog.open(EgresosComponent, {
        width: '50%',
        panelClass: 'divmodal',
        data: {title: 'Registro de ingresos', egreso: this._egreso},
        disableClose: true
      });
      dialogRef.afterClosed().subscribe(() => {
        console.log('The dialog was closed');
      });
    }
  }

  nuevacaja(){
    const dialogRef = this.dialog.open(CajaregComponent, {
      width: '700px',
      panelClass: 'divmodal',
      data: {title: 'Registro de caja'},
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(() => {
      console.log('The dialog was closed');
    });
  }


  configuracionInforme(){
    const dialogRef = this.dialog.open(ConfiguracionInformeComponent, {
      width: '80%',
      panelClass: 'divmodal',
      data: {title: 'Configuración Informe Mensual'},
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(() => {
      console.log('The dialog was closed');
    });
  }
}

