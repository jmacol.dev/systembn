import { MatDialog } from '@angular/material/dialog';
import { Injectable } from '@angular/core';
import { Usuario } from '../models/usuario';
import { ValidateService } from './validate.service';
import Swal from 'sweetalert2';
import axios from "axios";
import { environment } from "./../../environments/environment";
import { HeaderautorizaService } from './headerautoriza.service';
import { Router } from '@angular/router';
import { WorkerService } from './worker.service';
import { HttpClient } from '@angular/common/http';
import { DeviceDetectorService } from 'ngx-device-detector';
import { CryptService } from './crypt.service';
import { decode } from 'querystring';
@Injectable({
  providedIn: 'root'
})



export class SeguridadService {
  public namesystem: string = 'Sistema Integral para Iglesias';
  public keyrecaptcha: string = environment.SITEKEY
  public urlexterno: string = environment.URLEXT
  public backgroundlogin: string = 'https://'+this.urlexterno+'/images/default/fondologin.png'
  public logologin: string = 'https://'+this.urlexterno+'/images/default/logo.png'
  public headerlogin: string = 'https://'+this.urlexterno+'/images/default/logohead.pn'
  public carpeta: string = 'default'
  public moneda: any = 'S/ '
  public usuarioreg: Usuario = {
    id: 0,
    nombre: '',
    apellidoPaterno: '',
    apellidoMaterno: '',
    nombreCompleto: 'Nombre de usuario',
    usuario: '',
    password: '',
    iglesia: '',
    perfil: 'Administrador'
   };
   ip: string = '0.0.0.0'
   browser: string = ''
   device: string = ''
  public disload: boolean = false;
  constructor( public valserv: ValidateService,
    public hautserv: HeaderautorizaService,
    public workserv: WorkerService,
    private router: Router,
    public http: HttpClient,
    private deviceService: DeviceDetectorService,
    private dialogRef: MatDialog,
    public cryptserv: CryptService) {
      this.http.get("http://api.ipify.org/?format=json").subscribe((res:any)=>{
      this.ip = res.ip
      this.browser = navigator.userAgent
      this.device = this.deviceService.deviceType
    });

    if (location.hostname == 'localhost') {
      this.carpeta= 'localhost'
      this.namesystem = "Sistema Integral para Iglesias"
    } else {
      switch (location.hostname) {
        case 'ibfgracia.' + this.urlexterno:
          this.carpeta = 'ibfgracia';
          this.namesystem = "Iglesia Bautista Fundamental Gracia"
          break;
        case 'ibbuenasnuevas.' + this.urlexterno:
          this.carpeta = 'ibbuenasnuevas';
          this.namesystem = "Iglesia Bautista Fundamental Buenas Nuevas"
          break;
        default:
          this.carpeta = 'default'
          this.namesystem = "Sistema Integral para Iglesias"
          break;
      }
    }

    this.backgroundlogin = 'https://'+this.urlexterno+'/images/'+this.carpeta+'/fondologin.png'
    this.logologin = 'https://'+this.urlexterno+'/images/'+this.carpeta+'/logo.png'
    this.headerlogin = 'https://'+this.urlexterno+'/images/'+this.carpeta+'/logohead.png'
  }


  async login($usuario: string, $password: string, $recaptchakey: string){
    let $ban: boolean = this.valserv.validatelogin($usuario, $password);

    if($ban){
      let request ={
        usuario: $usuario ?? '',
        password: $password ?? '',
        recaptcha: $recaptchakey ?? '',
        ip: this.ip  ?? '0.0.0.0',
        browser: this.browser,
        device: this.device
      }
      await axios.post(environment.URLAPI + "login", request, {
      headers: this.hautserv.headers,
      })
      .then((response) => {
        this.disload = false;
        if (response.data.estado) {
          console.log(JSON.stringify(response.data.user[0]))
          Swal.fire(this.namesystem, response.data.mensaje, "success").then(()=>{
            response.data.user[0].password = ''
            sessionStorage.setItem("user", this.cryptserv.set(JSON.stringify(response.data.user[0])));
            sessionStorage.setItem("token", this.cryptserv.set(response.data.token))
            this.router.navigate(['/dashboard']);
          }).then(() => {
            this.valserv.showInfo(`Hoy están cumpliendo años 1 persona(s).` , this.namesystem)
            this.valserv.showSuccess(response.data.user[0].nombreCompleto, this.namesystem + '\n' + 'Bienvenido al Sistema')
          })

        }else{
          sessionStorage.removeItem("user");
          sessionStorage.removeItem("token");
          this.valserv.validaban = false;
          this.valserv.showError(response.data.mensaje, this.namesystem)
        }
      })
      .catch( (error) =>{
        this.disload = false;
        console.log(error);
      });
    }else{
      this.disload = false
    }
  }

  async logout(){
    console.log(sessionStorage.getItem('token'))
    Swal.fire({
      title: this.namesystem,
      text: '¿Está seguro de cerrar sesión?',
      icon: 'question',
      showCancelButton: true,
      confirmButtonText: 'Aceptar',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.value) {
        this.limpiarsesion()

      }
    })
  }

  // conexionactiva(): boolean {
  //   let $state: boolean = true;
  //   if (sessionStorage.getItem("token") !== '' && sessionStorage.getItem("token") !== null) {
  //     if (sessionStorage.getItem("user") !== '' && sessionStorage.getItem("user") !== null) {
  //       $state = true;
  //     }else{
  //       $state = false;
  //     }
  //   }else{
  //     $state = false;
  //   }
  //   if(!$state){
  //     this.limpiarsesion()
  //   }else{
  //     setInterval(() => {
  //       if(this.workserv.alive){
  //         this.workserv.notificaciones();
  //       }},5000);
  //   }
  //   return $state;
  // }

  limpiarsesion(){
    sessionStorage.removeItem("user");
    sessionStorage.removeItem("token");
    this.workserv.alive = false;
    this.dialogRef.closeAll();
    this.router.navigate(['/login']);

  }



}
