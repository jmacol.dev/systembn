import { DateAdapter, MAT_DATE_FORMATS } from '@angular/material/core';
import { AppDateAdapter, APP_DATE_FORMATS } from 'src/app/helpers/format-datepicker';
import { FormControl } from '@angular/forms';
import { ListService } from './../../../services/list.service';
import { FamiliaCbo } from './../../../models/familia';
import { CategoriaCbo } from './../../../models/categoria';
import { Component, Inject, OnInit, Output, EventEmitter } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConfiguracionService } from 'src/app/services/configuracion.service';
import { NewService } from 'src/app/services/new.service';
import { RegisterService } from 'src/app/services/register.service';

@Component({
  selector: 'app-dialogmiembroreg',
  templateUrl: './dialogmiembroreg.component.html',
  styleUrls: ['./dialogmiembroreg.component.sass'],
  providers: [
    {provide: DateAdapter, useClass: AppDateAdapter},
    {provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS}
  ]
})
export class DialogmiembroregComponent implements OnInit {
  @Output() openList = new EventEmitter();
  datacbo: CategoriaCbo[] = []
  etapa_lst: CategoriaCbo[] = []
  familia_lst: FamiliaCbo[] = []
  today: Date | undefined;
  date: any;
  constructor(
    public configserv: ConfiguracionService,
    private lstserv: ListService,
    public regserv: RegisterService,
    public dialogRef: MatDialogRef<DialogmiembroregComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { title: 'Registro de personas', id: '', listar: false}
  ) {
    this.regserv.miembro_reg = this.regserv.empty_miembro()
    this.date = new FormControl(new Date());
    this.today = new Date();
  this.initialiseInvites();
   }


   initialiseInvites() {
    // Set default values and re-fetch any data you need.

      this.lstserv.listado('list-etapas', '').then(dataaux=>{
        this.datacbo = dataaux;
        this.etapa_lst = this.datacbo.filter(item => item.superior == null)

      })


  }

  ngOnInit(): void {
    if (this.data.id != '') {
      this.lstserv.listado('get-miembro', '?id=' + this.data.id).then(dataaux => {
        let miembro = dataaux;

        this.regserv.miembro_reg = miembro;
        var fechanac = new Date(this.regserv.miembro_reg.fechanacimiento)
        this.regserv.miembro_reg.fechanacimiento = new Date(fechanac.setDate(fechanac.getDate() + 1))
        console.log(JSON.stringify(this.regserv.miembro_reg))

      })
    } else {
      this.regserv.miembro_reg = this.regserv.empty_miembro()
    }
  }

  seleccionarfechanacimiento() {
    this.configserv.datepipe.transform(this.regserv.miembro_reg.fechanacimiento, "YYYY-MM-dd")
  }



  guardarmiembro() {
    console.log(this.regserv.miembro_reg)
    let fechanacimiento = this.configserv.datepipe.transform(this.regserv.miembro_reg.fechanacimiento, "YYYY-MM-dd")
    this.regserv.registro('Registro de personas', 'register-miembros',
      {
      id: this.regserv.miembro_reg.id,
      nombre: this.regserv.miembro_reg.nombres,
      apellidopaterno: this.regserv.miembro_reg.apellidopaterno,
      apellidomaterno: this.regserv.miembro_reg.apellidomaterno,
      email: this.regserv.miembro_reg.email,
      telefono1: this.regserv.miembro_reg.telefono1,
      telefono2: this.regserv.miembro_reg.telefono2,
      miembro: this.regserv.miembro_reg.miembro,
      sexo: this.regserv.miembro_reg.sexo,
      clasificacionedad: this.regserv.miembro_reg.clasificacionedad,
      familia: this.regserv.miembro_reg.familia,
      fechanacimiento: fechanacimiento

    }).then(value=>{
      if (value) {
        if (this.data.listar) {
          this.lstserv.listar(this.openList.emit(null))
        }
        this.configserv.onNoClickDialog(this.dialogRef)
      }
    })

  }
}
