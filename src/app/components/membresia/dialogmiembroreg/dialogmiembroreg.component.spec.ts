import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogmiembroregComponent } from './dialogmiembroreg.component';

describe('DialogmiembroregComponent', () => {
  let component: DialogmiembroregComponent;
  let fixture: ComponentFixture<DialogmiembroregComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialogmiembroregComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogmiembroregComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
