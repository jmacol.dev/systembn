import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogfamiliaregComponent } from './dialogfamiliareg.component';

describe('DialogfamiliaregComponent', () => {
  let component: DialogfamiliaregComponent;
  let fixture: ComponentFixture<DialogfamiliaregComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialogfamiliaregComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogfamiliaregComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
