import { ListService } from './../../../services/list.service';
import { AppDateAdapter, APP_DATE_FORMATS } from './../../../helpers/format-datepicker';
import { DateAdapter, MAT_DATE_FORMATS } from '@angular/material/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConfiguracionService } from './../../../services/configuracion.service';
import { RegisterService } from './../../../services/register.service';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-dialogfamiliareg',
  templateUrl: './dialogfamiliareg.component.html',
  styleUrls: ['./dialogfamiliareg.component.sass'],
  providers: [
    {provide: DateAdapter, useClass: AppDateAdapter},
    {provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS}
  ]
})
export class DialogfamiliaregComponent implements OnInit {

  constructor(
    public configserv: ConfiguracionService,
    public regserv: RegisterService,
    public lstserv: ListService,
    public dialogRef: MatDialogRef<DialogfamiliaregComponent>,
    @Inject(MAT_DIALOG_DATA) public data: {title: 'Registro de familias', id: '', listar: false }
  ) {
    this.regserv.familia_reg = this.regserv.empty_familia()
  }

  ngOnInit(): void {
    console.log(this.data.id)
    if (this.data.id != '') {
      this.lstserv.listado('get-familia', '?id=' + this.data.id).then(dataaux => {
        let familia = dataaux;

        this.regserv.familia_reg = familia;
        console.log(JSON.stringify(this.regserv.familia_reg))

      })
    } else {
      this.regserv.familia_reg = this.regserv.empty_familia()
    }
  }

  guardarfamilia(){
    this.regserv.registro('Registro de familias', 'register-familias',
      {
      id: this.data.id,
      apellidopaterno: this.regserv.familia_reg.apellidopaterno,
      apellidomaterno: this.regserv.familia_reg.apellidomaterno

    }).then(value=>{
      if(value){
        this.configserv.onNoClickDialog(this.dialogRef)
      }
    })

  }

}
