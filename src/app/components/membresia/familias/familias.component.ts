import { PageEvent } from '@angular/material/paginator';
import { PaginationService } from 'src/app/services/pagination.service';
import { RegisterService } from 'src/app/services/register.service';
import { ListService } from './../../../services/list.service';
import { Router } from '@angular/router';
import { SeguridadService } from 'src/app/services/seguridad.service';
import { ControlesService } from './../../../services/controles.service';
import { SidenavService } from './../../../services/sidenav.service';
import { NewmodalService } from 'src/app/services/newmodal.service';
import { NewService } from './../../../services/new.service';
import { FormControl } from '@angular/forms';
import { Familia } from './../../../models/familia';
import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-familias',
  templateUrl: './familias.component.html',
  styleUrls: ['./familias.component.sass']
})
export class FamiliasComponent implements OnInit {
  public dataGlobal: Familia[] = []
  collectionSize = 0;
  pageEvent: PageEvent = new PageEvent;
  search:string = ""
  public banperm: boolean = false
  myControl = new FormControl();
  title: string = 'Registro de Familias';
  route: string = 'familias'
  modulo: string = 'MEMBRESIA';
  @ViewChild('drawer') sideNav: any;
  constructor(
    public nvoservc: NewService,
    public newmodal: NewmodalService,
    public sidenavservc: SidenavService,
    public lstserv: ListService,
    public regserv: RegisterService,
    public ctrlserv: ControlesService,
    public segserv:SeguridadService,
    private router: Router,
    public pageService: PaginationService,
  ) {
    this.pageEvent = this.pageService.inicializar()
    this.segserv.disload = false;
  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): any {
    this.sidenavservc.abrirmenu(this.sideNav);
    this.nvoservc.nuevo = () => {
      this.newmodal.nuevaFamilia('', true);
    };
    this.lstserv.listar = () => {
      console.log('lista familias');
      this.lista_familias()
    }
  }

  lista_familias(){
    this.lstserv.listado('list-familias-table', '').then(data => {
      console.log(JSON.stringify(data));
      if (data.length != this.dataGlobal.length) {
        console.log("pasa")
      this.dataGlobal = data
      this.pageService.coleccionsize = this.pageService.dataSource.length
      this. pageService.dataSource = data
      this.collectionSize = this.pageService.dataSource.length
      this.pageEvent.length = data.length
      this.pageService.actualizaTabla2(data, this.pageEvent);
    }
    })
  }

  applyFilter() {
    this.search = this.search.trim(); // Remove whitespace
    this.search = this.search.toLowerCase(); // Datasource defaults to lowercase matches
    let data = this.dataGlobal.filter(item => (item.apellidopaterno + ' ' + item.apellidomaterno).toLowerCase().includes(this.search));
    this.pageService.coleccionsize = this.pageService.dataSource.length
    this. pageService.dataSource = data
    this.collectionSize = this.pageService.dataSource.length
    this.pageEvent.length = data.length
    this.pageService.actualizaTabla2(data, this.pageEvent);
  }

  editarfamilia($id: string) {

    this.newmodal.nuevaFamilia($id, true);
  }

  eliminarfamilia($id: string, $state: number) {

    this.regserv.registro('Registro de personas', 'state-familias',
      {
      id: $id,
      activo:  $state

      }).then(value => {
      console.log(value)
        if (value) {
        this.dataGlobal = []
        this.lista_familias()
      }
    })
  }



}
