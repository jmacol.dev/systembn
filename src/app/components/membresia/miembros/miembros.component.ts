import { APP_DATE_FORMATS, AppDateAdapter } from 'src/app/helpers/format-datepicker';
import { DateAdapter, MAT_DATE_FORMATS } from '@angular/material/core';
import { NotificacionesService } from './../../../services/notificaciones.service';
import { ConfiguracionService } from './../../../services/configuracion.service';
import { ValidateService } from './../../../services/validate.service';
import { ExportarexcelService } from './../../../services/exportarexcel.service';
import { PageEvent } from '@angular/material/paginator';
import { PaginationService } from './../../../services/pagination.service';
import { RegisterService } from 'src/app/services/register.service';
import { ListService } from './../../../services/list.service';
import { NewmodalService } from 'src/app/services/newmodal.service';
import { Router } from '@angular/router';
import { SeguridadService } from 'src/app/services/seguridad.service';
import { ControlesService } from './../../../services/controles.service';
import { SidenavService } from './../../../services/sidenav.service';
import { NewService } from './../../../services/new.service';
import { Miembro, MiembroTable } from './../../../models/miembro';
import { FormControl } from '@angular/forms';
import { Component, OnInit, ViewChild } from '@angular/core';
import { jsPDF } from 'jspdf';
import autoTable from 'jspdf-autotable'

@Component({
  selector: 'app-miembros',
  templateUrl: './miembros.component.html',
  styleUrls: ['./miembros.component.sass'],
  providers: [
    {provide: DateAdapter, useClass: AppDateAdapter},
    {provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS}
  ]
})
export class MiembrosComponent implements OnInit {
  public dataGlobal: MiembroTable[] = []
  collectionSize = 0;
  pageEvent: PageEvent = new PageEvent;
  search:string = ""
  public banperm: boolean = false
  myControl = new FormControl();
  title: string = 'Registro de Personas';
  route: string = 'personas'
  modulo: string = 'MEMBRESIA';
  @ViewChild('drawer') sideNav: any;
  constructor(
    public nvoservc: NewService,
    public newmodal: NewmodalService,
    public sidenavservc: SidenavService,
    public lstserv: ListService,
    public regserv: RegisterService,
    public ctrlserv: ControlesService,
    public segserv:SeguridadService,
    private router: Router,
    public pageService: PaginationService,
    private excelserv: ExportarexcelService,
    public valserv: ValidateService,
    public configserv: ConfiguracionService
  ) {
      this.pageEvent = this.pageService.inicializar()
      this.segserv.disload = false;
  }

  ngOnInit(): void {
    this.lista_miembros()
  }

  ngAfterViewInit(): any {
    this.sidenavservc.abrirmenu(this.sideNav);
    this.nvoservc.nuevo = () => {
      this.newmodal.nuevoMiembro('', true);
    };
    this.lstserv.listar = () => {
      console.log('lista miembros');
      this.dataGlobal = []
      this.lista_miembros()
    }
    this.lstserv.excel = () => {
      console.log("exportar")
    this.exportExcel()
    }

    this.lstserv.pdf = () => {
      console.log("exportar pdf")
    this.exportPdf()
    }
  }

  dataForExcel:any[] = [];
  empPerformance:any[] = []
  exportExcel(){
    if(this.dataGlobal.length>0){
      this.empPerformance = [];
      this.dataGlobal.forEach((item) => {
        this.empPerformance.push({
          'APELLIDOS Y NOMBRES': item.apellidopaterno + ' ' + item.apellidomaterno + ' ' + item.nombres,
          "FECHA NACIMIENTO": this.configserv.datepipe.transform(item.fechanacimiento, "dd-MM-yyyy"), "EMAIL": item.email,
          "TELÉFONO 01": item.telefono1, "TELÉFONO 02": item.telefono2, "SEXO": item.sexo == 1 ? 'MASCULINO' : 'FEMENINO',
          "CLASIFICACIÓN EDAD": item.clasificacionedad, "FAMILIA": item.familia, "MEMBRESÍA": item.miembro == 1 ? 'SI' : 'NO'
        });
      });
      this.empPerformance.forEach((row: any) => {
        this.dataForExcel.push(Object.values(row))
      })
      let d = new Date();
      let fecha = this.configserv.datepipe.transform(d, "dd-MM-YYYY")
      let reportData = {
        title: 'Listado de Personas Registradas',
        data: this.dataForExcel,
        headers: Object.keys(this.empPerformance[0]),
        description: "Personas registradas | " + fecha
      }

      this.excelserv.exportExcelDiezmosOfrendas(reportData);
    } else {
      this.valserv.showError("No hay personas registradas.", this.modulo + ' - ' + this.title)
      // this.valserv.validaban = false;
      // this.valserv.validatext = "No hay personas registradas.";
      // this.valserv.cerrarmensaje()
    }
  }


  exportPdf() {
     let fecha = this.configserv.datepipe.transform(this.regserv.movimientocaja_reg.fecha, "dd-MM-YYYY")
  const lastPoint = { x: 0, y: 0 };
  const doc = new jsPDF('l', 'mm', [297, 210]);
  const header = 'LISTADO DE PERSONAS REGISTRADAS'
  const pageWidth = doc.internal.pageSize.getWidth();
  doc.setFontSize(10);
  doc.setFont("Helvetica", "bold");
  const headerWidth = doc.getTextDimensions(header).w;
  doc.text(header, (pageWidth - headerWidth) / 2, 15);

  var col = ['N°','APELLIDOS y NOMBRES', 'FECHA NACIMIENTO', 'EMAIL', 'TELÉFONO 01', 'TELÉFONO 02', 'SEXO', 'CLASIFICACIÓN EDAD', 'FAMILIA', 'MEMBRESÍA'];
  var rows:any = [];

   this.dataGlobal.forEach((item, index) => {
  // tslint:disable-next-line:max-line-length
    const temp = [(index + 1), item.apellidopaterno + ' ' + item.apellidomaterno + ' ' + item.nombres,
          this.configserv.datepipe.transform(item.fechanacimiento, "dd-MM-yyyy"), item.email,
          item.telefono1, item.telefono2, item.sexo == 1 ? 'MASCULINO' : 'FEMENINO',
          item.clasificacionedad, item.familia, item.miembro == 1 ? 'SI' : 'NO'];
    rows.push(temp);

  });



  autoTable(doc, {
    head: [col],
    body:
      rows
    , styles: { fontSize: 7, overflow: 'linebreak' }, startY: 20, columnStyles: { 0: { cellWidth: 10 }, 2: { halign: 'right' }, 3: { halign: 'right' }, 4: { halign: 'right' }, 5: { halign: 'right' }, 6: { halign: 'right' }, 7: { halign: 'right' } }
  })

  const footer = 'Sistema Integral - MEMBRESÍA';
  doc.setFontSize(7);
  doc.setTextColor('#cccccc');
  const footerWidth = doc.getTextDimensions(footer).w;
  doc.text(footer, (lastPoint.x - footerWidth), lastPoint.y + 30);
  doc.save('Membresias.pdf');
  }

  lista_miembros(){
    this.lstserv.listado('list-miembros-table', '?sexo=' + this.regserv.miembro_busq.sexo + '&clasificacionedad=' + this.regserv.miembro_busq.clasificacionedad + '&tipo=' + this.regserv.miembro_busq.tipo).then(data => {
      console.log(JSON.stringify(data));
      this.pageEvent = this.pageService.inicializar()
      if (data.length == 0) {
        this. pageService.dataSource = []
      }
      if (data.length != this.dataGlobal.length) {
        console.log("pasa")
      this.dataGlobal = data
      this.pageService.coleccionsize = this.pageService.dataSource.length
      this. pageService.dataSource = data
      this.collectionSize = this.pageService.dataSource.length
      this.pageEvent.length = data.length
      this.pageService.actualizaTabla2(data, this.pageEvent);
      }
    })
  }

  applyFilter() {
    this.search = this.search.toLowerCase(); // Datasource defaults to lowercase matches
    let data = this.dataGlobal.filter(item => (item.nombres + ' ' + item.apellidopaterno + ' ' + item.apellidomaterno).toLowerCase().includes(this.search));
    this.pageService.coleccionsize = this.pageService.dataSource.length
    this. pageService.dataSource = data
    this.collectionSize = this.pageService.dataSource.length
    this.pageEvent.length = data.length
    this.pageService.actualizaTabla2(data, this.pageEvent);
  }

  editarmiembro($id: string) {

    this.newmodal.nuevoMiembro($id, true);
  }

  eliminarmiembro($id: string, $state: number) {

    this.regserv.registro('Registro de personas', 'state-miembros',
      {
      id: $id,
      activo:  $state

      }).then(value => {
      console.log(value)
        if (value) {
        this.dataGlobal = []
        this.lista_miembros()
      }
    })
  }

}
