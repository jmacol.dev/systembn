import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ConfiguracionService } from 'src/app/services/configuracion.service';
import { ControlesService } from 'src/app/services/controles.service';
import { ListService } from 'src/app/services/list.service';
import { RegisterService } from 'src/app/services/register.service';
import { SeguridadService } from 'src/app/services/seguridad.service';
import { SidenavService } from 'src/app/services/sidenav.service';

import * as Highcharts from 'highcharts';
import Exporting from 'highcharts/modules/exporting';
import OfflineExporting from 'highcharts/modules/offline-exporting';
import ExportData from 'highcharts/modules/export-data';
import HighchartsMore from 'highcharts/highcharts-more';
Exporting(Highcharts);
OfflineExporting(Highcharts);
ExportData(Highcharts);
HighchartsMore(Highcharts);

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.sass']
})

export class DashboardComponent implements OnInit {
  Highcharts: typeof Highcharts = Highcharts;
  @ViewChild('drawer') sideNav: any;
  ipAddress: string = '0.0.0.0'
  constructor(
    public sidenavservc: SidenavService,
    public segserv: SeguridadService,
    private router: Router,
    public ctrlserv: ControlesService,
    public configserv: ConfiguracionService,
    public regserv: RegisterService,
    public listserv: ListService,

  ) {
    this.segserv.disload = false;
   }



  ngOnInit(): void {


  }



  ngAfterViewInit(): any {
    this.sidenavservc.abrirmenu(this.sideNav);
    this.generarReporte()
  }


  generarReporte() {
    Highcharts.chart('grafico-estadistico', {
    title: {
        text: 'Ingresos y Egresos - 3 últimos meses',
        align: 'center'
      },
      credits: {
        enabled: false,
      },
    xAxis: {
        categories: ['NOVIEMBRE 2022', 'DICIEMBRE 2022', 'ENERO 2023']
    },
    yAxis: {
        title: {
            text: ' Nuevos Soles'
        }
    },
    tooltip: {
        valueSuffix: ' Nuevos Soles'
    },

    series: [{
        type: 'column',
        name: 'INGRESOS',
        data: [590, 830, 650]
    }, {
        type: 'column',
        name: 'EGRESOS',
        data: [240, 790, 720]
    },  {
        type: 'line',
        name: 'SALDO',
        data: [350, 40, -70],
        marker: {
            lineWidth: 2,
            lineColor: '#DE5A3E',
            fillColor: '#DD7A66'
        }
      },
      ], exporting: {
      enabled: false,
        showTable: false,
    }
});
  }

}
