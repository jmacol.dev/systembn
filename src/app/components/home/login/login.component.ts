import { AfterViewInit, Component, Directive, ElementRef, OnInit, ViewChildren } from '@angular/core';
import { SeguridadService } from 'src/app/services/seguridad.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { ActivoGuard } from 'src/app/guards/activo.guard';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})

export class LoginComponent implements OnInit  {
  public anio: string = new Date().getFullYear().toString();
  public usuario: string = ''
  public password: string = ''
  hide = true;
  constructor(
    public segserv: SeguridadService,
    private router: Router,
    private guard: ActivoGuard
  ) {
    var styles = "https://ibfgracia.org/styles/"+segserv.carpeta+"/login.scss";
    var newSS=document.createElement('link');
    newSS.rel='stylesheet';
    newSS.href=unescape(styles);
    document.getElementsByTagName("head")[0].appendChild(newSS);
    console.log(this.guard.isLoggedIn())
    if(this.guard.isLoggedIn()) this.router.navigate(['/dashboard']);
  }

  ngOnInit(): void {

  }

  login($recaptchakey: string){
    this.segserv.disload = true;
    this.segserv.login(this.usuario, this.password, $recaptchakey);
  }

}
