import { SeguridadService } from 'src/app/services/seguridad.service';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';
import { diezmoofrenda } from 'src/app/models/diezmosofrenda';
import { ConfiguracionService } from 'src/app/services/configuracion.service';
import { ListService } from 'src/app/services/list.service';
import { PaginationService } from 'src/app/services/pagination.service';
import { RegisterService } from 'src/app/services/register.service';
import { CtrldialogeliminarComponent } from '../ctrldialogeliminar/ctrldialogeliminar.component';

@Component({
  selector: 'app-ctrldialogdetallegrupo',
  templateUrl: './ctrldialogdetallegrupo.component.html',
  styleUrls: ['./ctrldialogdetallegrupo.component.sass']
})
export class CtrldialogdetallegrupoComponent implements OnInit {
  dataGlobal: diezmoofrenda[] = [];
  collectionSize = 0;
  pageEvent: PageEvent = new PageEvent;

  constructor(
    public dialogRef: MatDialogRef<CtrldialogeliminarComponent>,
    public lstserv: ListService,
    public pageService: PaginationService,
    public regserv: RegisterService,
    public configserv: ConfiguracionService,
    public segserv: SeguridadService,
    @Inject(MAT_DIALOG_DATA) public data: {title: 'Detalle', grupo: ''}
  ) {

  }

  ngOnInit(): void {
    this.lstserv.listado('list-detallegrupo','?grupo='+this.data.grupo).then(data=>{
    this.dataGlobal = data
    // this.pageService.coleccionsize = this.pageService.dataSource.length
    // this. pageService.dataSource = data
    // this.pageEvent.length = data.length
    // this.collectionSize = this.pageService.dataSource.length
    // this.pageService.actualizaTabla2(data, this.pageEvent);
  })
  }

}
