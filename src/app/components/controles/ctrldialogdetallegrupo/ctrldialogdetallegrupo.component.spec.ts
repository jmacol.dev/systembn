import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CtrldialogdetallegrupoComponent } from './ctrldialogdetallegrupo.component';

describe('CtrldialogdetallegrupoComponent', () => {
  let component: CtrldialogdetallegrupoComponent;
  let fixture: ComponentFixture<CtrldialogdetallegrupoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CtrldialogdetallegrupoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CtrldialogdetallegrupoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
