import { SeguridadService } from 'src/app/services/seguridad.service';
import { Component, OnInit } from '@angular/core';
import { ControlesService } from 'src/app/services/controles.service';

@Component({
  selector: 'app-ctrlsaldoscaja',
  templateUrl: './ctrlsaldoscaja.component.html',
  styleUrls: ['./ctrlsaldoscaja.component.sass']
})
export class CtrlsaldoscajaComponent implements OnInit {

  constructor(
    public ctrlserv: ControlesService,
    public segserv: SeguridadService
  ) { }

  ngOnInit(): void {
  }

}
