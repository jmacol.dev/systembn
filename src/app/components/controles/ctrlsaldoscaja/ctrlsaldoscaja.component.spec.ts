import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CtrlsaldoscajaComponent } from './ctrlsaldoscaja.component';

describe('CtrlsaldoscajaComponent', () => {
  let component: CtrlsaldoscajaComponent;
  let fixture: ComponentFixture<CtrlsaldoscajaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CtrlsaldoscajaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CtrlsaldoscajaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
