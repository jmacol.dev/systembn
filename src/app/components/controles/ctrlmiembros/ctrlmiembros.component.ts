import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { Miembro, MiembroCbo } from 'src/app/models/miembro';
import { ControlesService } from 'src/app/services/controles.service';
import { NewmodalService } from 'src/app/services/newmodal.service';
import {map, startWith} from 'rxjs/operators';
import { identifierModuleUrl } from '@angular/compiler';
import { ListService } from 'src/app/services/list.service';
import { RegisterService } from 'src/app/services/register.service';
@Component({
  selector: 'app-ctrlmiembros',
  templateUrl: './ctrlmiembros.component.html',
  styleUrls: ['./ctrlmiembros.component.sass']
})
export class CtrlmiembrosComponent implements OnInit {
  @Input() descripcionctrl: string = ''
  @Input() tipo!: any
  @Input() mostrar:boolean = true
  @Input() testigo:boolean = false
  @Input() gral!: any
  @Input() responsable!:any
  icon !:string

  myControl = new FormControl();
  options: MiembroCbo[] = []
  classtxt = 'col-lg-9 col-md-9 col-8'
  constructor(
    public nvomodservic: NewmodalService,
    public ctrlserv: ControlesService,
    private lstserv: ListService,
    private regserv: RegisterService
  ) {
    console.log("responsable " + this.responsable)

  }
  clear(ctrl:FormControl){
    ctrl.setValue(null);
    this.listado_control()
    this.myControl = new FormControl(this.responsable);
  }

  filteredOptions!: Observable<MiembroCbo[]>;
  ngOnInit() {
    this.listado_control()
    this.myControl = new FormControl(this.responsable);
    if(!this.mostrar){
      this.classtxt = "col-12"
    }else{
      this.classtxt = 'col-lg-9 col-md-9 col-8'
    }
  }

  listado_control(){
    if(this.tipo == 0){
      this.icon = 'user-plus';
      this.lista_miembros(this.gral)
    }else if(this.tipo == 1){
      this.icon = 'user-check';
      this.lista_autorizacaja()
    }
  }

  handleModelChange(e:any){
    if(e == ''){
      if(this.testigo){
        this.regserv.movimientocaja_reg.autorizacion =  ''
      }else{
        this.regserv.movimientocaja_reg.responsable =  ''
      }
    }
  }
  lista_miembros($gral:any){
    this.lstserv.listado('list-miembros','?tipo='+this.tipo).then(data=>{
      let miemlst: MiembroCbo[] = data;
      if ($gral){
        this.options  = miemlst.sort();
      }else{
        this.options  = miemlst.filter(item => item.nombreCompleto.trim() != "General").sort()
      }
      this.cargardata()
    })
  }

  lista_autorizacaja(){
    this.lstserv.listado('list-miembros','?tipo='+this.tipo).then(data=>{
      this.options = data.sort();
      this.cargardata()
    })
  }

  cargardata(){
    this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(value => typeof value === 'string' ? value : value.nombreCompleto),
        map(nombreCompleto => nombreCompleto ? this._filter(nombreCompleto) : this.options.slice())
      );
  }

  displayFn(user: MiembroCbo): string {
    return user && user.nombreCompleto ? user.nombreCompleto : '';
  }

  private _filter(name: string): MiembroCbo[] {
    const filterValue = name.toLowerCase();

    return this.options.filter(option => option.nombreCompleto.toLowerCase().includes(filterValue) );
  }


  customerOnChange($id:any, $tipo: any){
    console.log($id)
    console.log($tipo)
      if($tipo == 1) {
        this.regserv.movimientocaja_reg.autorizacion = $id
      }
      else {
        if(this.testigo){

          this.regserv.movimientocaja_reg.autorizacion =  $id

        }else{
          this.regserv.movimientocaja_reg.responsable =  $id
        }
      }

  }




}
