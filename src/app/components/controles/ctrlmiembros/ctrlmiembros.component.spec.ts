import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CtrlmiembrosComponent } from './ctrlmiembros.component';

describe('CtrlmiembrosComponent', () => {
  let component: CtrlmiembrosComponent;
  let fixture: ComponentFixture<CtrlmiembrosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CtrlmiembrosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CtrlmiembrosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
