import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CtrldialogeliminarComponent } from './ctrldialogeliminar.component';

describe('CtrldialogeliminarComponent', () => {
  let component: CtrldialogeliminarComponent;
  let fixture: ComponentFixture<CtrldialogeliminarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CtrldialogeliminarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CtrldialogeliminarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
