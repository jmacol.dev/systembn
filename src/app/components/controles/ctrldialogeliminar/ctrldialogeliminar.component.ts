import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConfiguracionService } from 'src/app/services/configuracion.service';
import { ListService } from 'src/app/services/list.service';
import { RegisterService } from 'src/app/services/register.service';

@Component({
  selector: 'app-ctrldialogeliminar',
  templateUrl: './ctrldialogeliminar.component.html',
  styleUrls: ['./ctrldialogeliminar.component.sass']
})
export class CtrldialogeliminarComponent implements OnInit {
  @Output() openList = new EventEmitter();
  public motivoeliminacion: string = ''
  constructor(
    public dialogRef: MatDialogRef<CtrldialogeliminarComponent>,
    public configserv: ConfiguracionService,
    public regserv: RegisterService,
    public lstserv: ListService,
    @Inject(MAT_DIALOG_DATA) public data: {title: 'Eliminar', id: '', descripcion: '', route: ''}
  ) { }

  ngOnInit(): void {
  }

  eliminar(){
    let request = {
      id: this.data.id,
      motivo: this.motivoeliminacion
    }

    this.regserv.actualizar(this.data.title, this.data.route, request).then(value=>{
      if(value){
        this.lstserv.listar(this.openList.emit(null))
        this.configserv.onNoClickDialog(this.dialogRef);
      }
    })
  }

}
