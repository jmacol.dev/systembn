import { startWith, map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { RegisterService } from 'src/app/services/register.service';
import { ListService } from './../../../services/list.service';
import { ControlesService } from './../../../services/controles.service';
import { NewmodalService } from './../../../services/newmodal.service';
import { FamiliaCbo } from './../../../models/familia';
import { FormControl } from '@angular/forms';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-ctrlfamilias',
  templateUrl: './ctrlfamilias.component.html',
  styleUrls: ['./ctrlfamilias.component.sass']
})
export class CtrlfamiliasComponent implements OnInit {
   @Input() descripcionctrl: string = ''
    myControl = new FormControl();
  options: FamiliaCbo[] = []
  @Input() mostrar:boolean = true
  icon !:string
  classtxt = 'col-lg-7 col-md-7 col-7'
  constructor(
    public nvomodservic: NewmodalService,
    public ctrlserv: ControlesService,
    private lstserv: ListService,
    private regserv: RegisterService
  ) { }

  filteredOptions!: Observable<FamiliaCbo[]>;
  ngOnInit(): void {
    this.listado_control()
    if(!this.mostrar){
      this.classtxt = "col-12"
    }else{
      this.classtxt = 'col-lg-7 col-md-7 col-7'
    }
  }

  listado_control(){
    this.icon = 'users';
    this.lista_familias()
  }

  handleModelChange(e:any){
    if(e == ''){
      this.regserv.miembro_reg.familia =  ''
    }
  }

  lista_familias(){
    this.lstserv.listado('list-familias','').then(data=>{
      let familialst: FamiliaCbo[] = data;
      this.options  = familialst.sort();
      this.cargardata()
    })
  }

  cargardata(){
    this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(value => typeof value === 'string' ? value : value.nombreCompleto),
        map(nombreCompleto => nombreCompleto ? this._filter(nombreCompleto) : this.options.slice())
      );
  }

  displayFn(user: FamiliaCbo): string {
    return user && user.nombreCompleto ? user.nombreCompleto : '';
  }

  private _filter(name: string): FamiliaCbo[] {
    const filterValue = name.toLowerCase();

    return this.options.filter(option => option.nombreCompleto.toLowerCase().includes(filterValue) );
  }

  customerOnChange($id:any){
      this.regserv.miembro_reg.familia = $id
  }

}
