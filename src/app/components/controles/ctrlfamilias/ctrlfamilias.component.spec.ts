import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CtrlfamiliasComponent } from './ctrlfamilias.component';

describe('CtrlfamiliasComponent', () => {
  let component: CtrlfamiliasComponent;
  let fixture: ComponentFixture<CtrlfamiliasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CtrlfamiliasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CtrlfamiliasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
