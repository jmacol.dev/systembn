import { usuarioTable } from './../../../models/usuario';
import { MAT_DATE_FORMATS, DateAdapter } from '@angular/material/core';
import { AppDateAdapter, APP_DATE_FORMATS } from './../../../helpers/format-datepicker';
import { ConfiguracionService } from './../../../services/configuracion.service';
import { ValidateService } from './../../../services/validate.service';
import { ExportarexcelService } from './../../../services/exportarexcel.service';
import { PaginationService } from './../../../services/pagination.service';
import { Router } from '@angular/router';
import { SeguridadService } from './../../../services/seguridad.service';
import { ControlesService } from './../../../services/controles.service';
import { RegisterService } from './../../../services/register.service';
import { ListService } from './../../../services/list.service';
import { SidenavService } from './../../../services/sidenav.service';
import { NewmodalService } from './../../../services/newmodal.service';
import { NewService } from './../../../services/new.service';
import { FormControl } from '@angular/forms';
import { PageEvent } from '@angular/material/paginator';
import { MiembroTable } from './../../../models/miembro';
import { Component, OnInit, ViewChild } from '@angular/core';
import { jsPDF } from 'jspdf';
import autoTable from 'jspdf-autotable'


@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.sass'],
  providers: [
    {provide: DateAdapter, useClass: AppDateAdapter},
    {provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS}
  ]
})
export class UsuariosComponent implements OnInit {
  public dataGlobal: usuarioTable[] = []
  collectionSize = 0;
  pageEvent: PageEvent = new PageEvent;
  search:string = ""
  public banperm: boolean = false
  myControl = new FormControl();
  title: string = 'Registro de Usuarios';
  route: string = 'usuarios'
  modulo: string = 'SEGURIDAD';
  @ViewChild('drawer') sideNav: any;
  constructor(
    public nvoservc: NewService,
    public newmodal: NewmodalService,
    public sidenavservc: SidenavService,
    public lstserv: ListService,
    public regserv: RegisterService,
    public ctrlserv: ControlesService,
    public segserv:SeguridadService,
    private router: Router,
    public pageService: PaginationService,
    private excelserv: ExportarexcelService,
    public valserv: ValidateService,
    public configserv: ConfiguracionService
  ) {
    this.pageEvent = this.pageService.inicializar()
    this.segserv.disload = false;
    this.lista_usuarios()
  }

  ngOnInit(): void {
  }
  ngAfterViewInit(): any {
    this.sidenavservc.abrirmenu(this.sideNav);
    this.nvoservc.nuevo = () => {
      this.newmodal.nuevoUsuario('', true);
    };
    this.lstserv.listar = () => {
      console.log('lista usuarios');
      this.dataGlobal = []
      this.lista_usuarios()
    }
    this.lstserv.excel = () => {
      console.log("exportar")
    // this.exportExcel()
    }

    this.lstserv.pdf = () => {
      console.log("exportar pdf")
    // this.exportPdf()
    }
  }

  lista_usuarios() {
     this.lstserv.listado('list-usuario', '').then(data => {
    console.log(JSON.stringify(data))
    if(data.length != this.dataGlobal.length){
      this.dataGlobal = data
    }

    this.pageService.dataSource = this.dataGlobal
    console.log(JSON.stringify(this.pageService.dataSource ))
    this.pageService.coleccionsize = this.pageService.dataSource.length
    this.pageEvent.length = data.length
    this.collectionSize = this.pageService.dataSource.length
    this.pageService.actualizaTabla2(this.dataGlobal, this.pageEvent);
  })
   }
  applyFilter() {
    this.search = this.search.toLowerCase(); // Datasource defaults to lowercase matches
    let data = this.dataGlobal.filter(item => (item.nombreCompleto).toLowerCase().includes(this.search));
    this.pageService.coleccionsize = this.pageService.dataSource.length
    this. pageService.dataSource = data
    this.collectionSize = this.pageService.dataSource.length
    this.pageEvent.length = data.length
    this.pageService.actualizaTabla2(data, this.pageEvent);
  }

  eliminarusuario($id: string, $state: any) {

  }

  editarusuario($id: string) {

  }

  permisosusuario($id: string) {

  }

  accesosOpciones($id: string) {

  }

}
