import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ValidateService } from './../../../../services/validate.service';
import { ListService } from './../../../../services/list.service';
import { RegisterService } from './../../../../services/register.service';
import { ConfiguracionService } from './../../../../services/configuracion.service';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-registro-usuario',
  templateUrl: './registro-usuario.component.html',
  styleUrls: ['./registro-usuario.component.sass']
})
export class RegistroUsuarioComponent implements OnInit {
  emailst: boolean = true
  constructor(
    public configserv: ConfiguracionService,
    public regserv: RegisterService,
    private listserv: ListService,
    private valserv:ValidateService,
    public dialogRef: MatDialogRef<RegistroUsuarioComponent>,
    @Inject(MAT_DIALOG_DATA) public data: {title: 'Registro de usuarios', id: '', listar: false }
  ) { }

  ngOnInit(): void {
  }

}
