import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Categoria } from 'src/app/models/categoria';
import { ConfiguracionService } from 'src/app/services/configuracion.service';
import { ControlesService } from 'src/app/services/controles.service';
import { ListService } from 'src/app/services/list.service';
import { NewService } from 'src/app/services/new.service';
import { PaginationService } from 'src/app/services/pagination.service';
import { RegisterService } from 'src/app/services/register.service';
import { SeguridadService } from 'src/app/services/seguridad.service';
import { SidenavService } from 'src/app/services/sidenav.service';
import Swal from 'sweetalert2';



@Component({
  selector: 'app-categorias',
  templateUrl: './categorias.component.html',
  styleUrls: ['./categorias.component.sass']
})
export class CategoriasComponent implements OnInit {

  modulo: string = 'CONFIGURACION';
  dataSource: Categoria[] = [];
  title: string = 'Categorías y subcategorías';
  route: string = 'categorias'
  isMobile = false;
  myControl = new FormControl();
  options: [] | undefined;
  filteredOptions: Observable<any> | undefined;
  @ViewChild('drawer') sideNav: any;

  displayedColumns: string[] = [
    'name',
    'diezmo',
    'ofrenda',
    'misiones',
    'misericordia',
    'protemplo',
    'total'
  ];
  dataglobal: Categoria[] = [];
  page = 1;
  pageSize = 5;
  pageEvent: PageEvent = new PageEvent;
  constructor(
    // public router: Router,
    private sidenavservc: SidenavService,
    public configservc: ConfiguracionService,
    public regserv: RegisterService,
    public nvoservc: NewService,
    private listserv: ListService,
    public dialog: MatDialog,
    private segserv: SeguridadService,
    public pageService: PaginationService,
    public ctrlserv: ControlesService,
    private router: Router
  ) {
    this.configservc.listado_categoria(this.configservc.idcateg.trim(), this.configservc.idsuperiorsub)
  }

  ngOnInit(): void {
    console.log('cargando componenete');
  }

  estado($estado: number, $categ: Categoria){
    let $request ={
      "id" : $categ.id,
      "activo": $estado
    }
    Swal.fire({
      title: this.segserv.namesystem,
      text: $estado == 0 ?'¿Está seguro dar de baja al registro?' : 'Está seguro de activar este registro',
      icon: 'question',
      showCancelButton: true,
      confirmButtonText: 'Aceptar',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.value) {

        if(!this.configservc.stcat){
          this.regserv.actualizar('Registro de categoría', 'state-categoria', $request).then(data =>{
            if(data) this.recargar_tabla(true, $categ);
          })
        }else{
          this.regserv.registro('Registro de subcategoría', 'state-subcategoria', $request).then(data =>{
            if(data) this.recargar_tabla(false, $categ);
          })
        }
      }
    })
  }

  recargar_tabla($state: boolean, $categ: Categoria){
    let $request = '?activo=-1';
    if($state){
      this.listserv.listado('list-categoria', $request).then(data =>{
      this.configservc.cargar_datacombo(data, $categ.superior, '');
    })
    }else{
      this.listserv.listado('list-subcategoria', $request).then(data =>{
      this.configservc.idcateg = $categ.idcategoria
      this.configservc.idsuperiorsub = $categ.superior
      this.configservc.cargar_datacombo(data, $categ.idcategoria, $categ.superior);
    })
    }
  }

  listado_categoriacombo(){
    let $request = '?activo=-1';
    this.listserv.listado('list-categoria', $request).then(data =>{

      this.configservc.combocategoria = data;
    })
  }
  ngAfterViewInit(): any {
    this.sidenavservc.abrirmenu(this.sideNav);
    this.nvoservc.nuevo = () => {
      this.nvoservc.nuevaCategoria(false, null);
    };
    this.configservc.seleccioncategoria();
    this.listserv.listar = () =>{
      this.configservc.listado_categoria(this.configservc.idcateg.trim(), this.configservc.idsuperiorsub)
    }

  }

}
