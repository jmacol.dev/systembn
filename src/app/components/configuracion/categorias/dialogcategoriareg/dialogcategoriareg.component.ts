import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Categoria } from 'src/app/models/categoria';
import { ConfiguracionService } from 'src/app/services/configuracion.service';
import { ListService } from 'src/app/services/list.service';
import { NewService } from 'src/app/services/new.service';
import { RegisterService } from 'src/app/services/register.service';
import { ValidateService } from 'src/app/services/validate.service';

@Component({
  selector: 'app-dialogcategoriareg',
  templateUrl: './dialogcategoriareg.component.html',
  styleUrls: ['./dialogcategoriareg.component.sass']
})
export class DialogcategoriaregComponent implements OnInit {

  constructor(
    public configserv: ConfiguracionService,
    public regserv: RegisterService,
    private listserv: ListService,
    private valserv:ValidateService,
    public dialogRef: MatDialogRef<DialogcategoriaregComponent>,
    @Inject(MAT_DIALOG_DATA) public data: {title: 'Registro de ' }
  ) { }

  ngOnInit(): void {
  }

  registrar(){
    let $request ={
      "id" :'',
      "categoria": '',
      "superior": '',
      "descripcion":''
    }

    let $ban: boolean = true
    if(this.configserv.stcat){
      $request = {
        "id": this.regserv.categoria_reg.id,
        "categoria": this.regserv.categoria_reg.idcategoria ,
        "superior": this.regserv.categoria_reg.superior ,
        "descripcion": this.regserv.categoria_reg.descripcion
      }
      $ban = this.valserv.validatesubcategoria($request.descripcion, $request.categoria, ' Configuración', 'Registro de Categorías y Subactegorías');
    }else{
      $request = {
          "id": this.regserv.categoria_reg.id,
          "superior": this.regserv.categoria_reg.idcategoria ,
          "categoria": '',
          "descripcion": this.regserv.categoria_reg.descripcion
      }
      $ban = this.valserv.validatecategoria($request.descripcion, ' Configuración', 'Registro de Categorías y Subactegorías');
    }

    if($ban){
      if(!this.configserv.stcat){

        if(this.regserv.categoria_reg.id.trim() == ''){
          this.regserv.registro('Registro de categoría', 'insert-categoria', $request).then(data =>{
            if(data) this.recargar_tabla(true);
          })
        }else{
          this.regserv.actualizar('Registro de categoría', 'update-categoria', $request).then(data =>{
            if(data) this.recargar_tabla(true);
          })
        }

    }else{
      if(this.regserv.categoria_reg.id.trim() == ''){
        this.regserv.registro('Registro de subcategoría', 'insert-subcategoria', $request).then(data =>{
          if(data) this.recargar_tabla(false);
        })
      }else{
        this.regserv.actualizar('Registro de subcategoría', 'update-subcategoria', $request).then(data =>{
          if(data) this.recargar_tabla(false);
        })
      }
    }
  }
  }



   recargar_tabla($state: boolean){
     if($state){
      let $request = '?activo=-1';
      this.listserv.listado('list-categoria', $request).then(data =>{
        this.configserv.cargar_datacombo(data, this.regserv.categoria_reg.superior, '');
        this.configserv.onNoClickDialog(this.dialogRef)
      })
    }else{
      let $request = '?activo=-1';
      this.listserv.listado('list-subcategoria', $request).then(data =>{
        this.configserv.idcateg = this.regserv.categoria_reg.idcategoria
        this.configserv.idsuperiorsub = this.regserv.categoria_reg.superior
        this.configserv.cargar_datacombo(data, this.regserv.categoria_reg.idcategoria, this.regserv.categoria_reg.superior);
        this.configserv.onNoClickDialog(this.dialogRef)
      })
    }
   }

}
