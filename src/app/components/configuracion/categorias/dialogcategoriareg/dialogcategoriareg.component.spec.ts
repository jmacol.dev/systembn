import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogcategoriaregComponent } from './dialogcategoriareg.component';

describe('DialogcategoriaregComponent', () => {
  let component: DialogcategoriaregComponent;
  let fixture: ComponentFixture<DialogcategoriaregComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialogcategoriaregComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogcategoriaregComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
