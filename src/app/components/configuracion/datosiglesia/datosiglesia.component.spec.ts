import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DatosiglesiaComponent } from './datosiglesia.component';

describe('DatosiglesiaComponent', () => {
  let component: DatosiglesiaComponent;
  let fixture: ComponentFixture<DatosiglesiaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DatosiglesiaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DatosiglesiaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
