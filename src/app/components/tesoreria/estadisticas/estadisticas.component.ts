import { ListService } from './../../../services/list.service';
import { NewService } from './../../../services/new.service';
import { SidenavService } from './../../../services/sidenav.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import * as Highcharts from 'highcharts';
import Exporting from 'highcharts/modules/exporting';
import OfflineExporting from 'highcharts/modules/offline-exporting';
import ExportData from 'highcharts/modules/export-data';
import HighchartsMore from 'highcharts/highcharts-more';

Exporting(Highcharts);
OfflineExporting(Highcharts);
ExportData(Highcharts);
HighchartsMore(Highcharts);
@Component({
  selector: 'app-estadisticas',
  templateUrl: './estadisticas.component.html',
  styleUrls: ['./estadisticas.component.sass']
})
export class EstadisticasComponent implements OnInit {

  title: string = 'Estadísticas';
  route: string = 'estadisticas'
  modulo: string = 'TESORERIA';

  Highcharts: typeof Highcharts = Highcharts;
  meses: any = [
            'ENERO',
            'FEBRERO',
            'MARZO',
            'ABRIL',
            'MAYO',
            'JUNIO',
            'JULIO',
            'AGOSTO',
            'SETIEMBRE',
            'OCTUBRE',
            'NOVIEMBRE',
            'DICIEMBRE'
  ]
  @ViewChild('drawer') sideNav: any;
  constructor(
    private sidenavservc: SidenavService,
    private nvoservc: NewService,
    public lstserv: ListService
  ) { }

  ngOnInit(): void {
  }

  ngAfterViewInit(): any {
    this.sidenavservc.abrirmenu(this.sideNav);
    this.nvoservc.nuevo = () => {
    };
    this.lstserv.listar = () => {
      console.log(this.lstserv.opcionreporte)
      let table = document.getElementsByClassName("highcharts-data-table")
      if(table[0] != undefined) table[0].remove()
      switch (this.lstserv.opcionreporte.toString()) {
        case '1': this.generarReporte1(); break;
        case '2': this.generarReporte2(); break;
        case '3': this.generarReporte3(); break;
        case '4': this.generarReporte4(); break;
        case '5': this.generarReporte5(); break;
      }

    }
  }

  private getRandomNumber(min: number, max: number): number {
    return Math.floor(Math.random() * (max - min + 1) + min)
  }


  generarReporte1() {
    Highcharts.chart('grafico-estadistico', {
    title: {
        text: 'Reporte Ingresos y Egresos',
        align: 'center'
      },
      credits: {
        enabled: false,
      },
    xAxis: {
        categories: this.meses
    },
    yAxis: {
        title: {
            text: ' Nuevos Soles'
        }
    },
    tooltip: {
        valueSuffix: ' Nuevos Soles'
    },

    series: [{
        type: 'column',
        name: 'INGRESOS',
        data: [590, 830, 650, 2680.5, 1840, 590, 830, 650, 2680.5, 1840, 2680.5, 1840]
    }, {
        type: 'column',
        name: 'EGRESOS',
        data: [240, 790, 720, 2400, 1670, 240, 790, 720, 2400, 1670,2400, 1670]
    },  {
        type: 'line',
        name: 'SALDO',
        data: [350, 40, -70, 280.5, 170, 350, 40, -70, 280.5, 170, 280.5, 170],
        marker: {
            lineWidth: 2,
            lineColor: '#DE5A3E',
            fillColor: '#DD7A66'
        }
      },
    //  {
    //     type: 'pie',
    //     name: 'Total',
    //     data: [{
    //         name: 'SALDO',
    //         y: 170,
    //         color: '#DD7A66',
    //         dataLabels: {
    //             enabled: true,
    //             distance: -50,
    //             format: 'S/ {point.total}',
    //             style: {
    //               fontSize: '15px',
    //               color:  '#191718'
    //             }
    //         }
    //     }
    //     //   , {
    //     //     name: 'EGRESOS',
    //     //     y: 15710,
    //     //     // color: Highcharts.getOptions().colors[1] // 2021 color
    //     // }
    //   ],
    //     center: [75, 65],
    //     size: 100,
    //     innerSize: '70%',
    //     showInLegend: false,
    //     dataLabels: {
    //         enabled: false
    //     }
    //     }
      ], exporting: {
      enabled: true,
        showTable: false,
    }
});
  }
  generarReporte2() {
    let date = new Date();
    const data: any = [];
    let monto: any = []
    monto = [];
    let rows: any = {}
    for (let i = 0; i < 12; i++) {
      monto.push(this.getRandomNumber(0, 1000))
    }
    rows ={
        name: 'DIEZMOS',
        data: monto,
    };
    data.push(rows)
    monto = [];
    for (let i = 0; i < 12; i++) {
      monto.push(this.getRandomNumber(0, 1000))
    }
    rows ={
        name: 'OFRENDAS',
        data: monto,
    };
    data.push(rows)
    monto = [];
    for (let i = 0; i < 12; i++) {
      monto.push(this.getRandomNumber(0, 1000))
    }
    rows ={
        name: 'MISIONES',
        data: monto,
    };
    data.push(rows)
    monto = [];
    for (let i = 0; i < 12; i++) {
      monto.push(this.getRandomNumber(0, 1000))
    }
    rows ={
        name: 'MISERICORDIA',
        data: monto,
    };
    data.push(rows)
    monto = [];
    for (let i = 0; i < 12; i++) {
      monto.push(this.getRandomNumber(0, 1000))
    }
    rows ={
        name: 'PRO-TEMPLO',
        data: monto,
    };
    data.push(rows)
    console.log(JSON.stringify(data))

    const chart = Highcharts.chart('grafico-estadistico' as any, {
      chart: {
        type: 'column',
      },
      title: {
        text: 'Reporte de Ingresos por Rubro',
      },
      credits: {
        enabled: false,
      },
      legend: {
        enabled: true,
      },
      yAxis: {
        min: 0,
        title: 'Meses',
      },
      xAxis: {
        type: 'category',
        labels: {
            rotation: 0,
            style: {
                fontSize: '11px',
                fontFamily: 'Verdana, sans-serif'
            }
        },
        categories: this.meses,
        crosshair: true
      },
      tooltip: {
        headerFormat: `<div>Mes: {point.key}</div>`,
        pointFormat: `<div>{series.name}: {point.y:.2f}</div>`,
        shared: true,
        useHTML: true,
      },
      plotOptions: {
        bar: {
          dataLabels: {
            enabled: true,
          },
        },
      },
      series: data,
        // {
        // name: 'Monto',
        // data ,
        dataLabels: {
            enabled: true,
            rotation: -90,
            color: '#FFFFFF',
            align: 'right',
            format: '{point.y:.2f}', // one decimal
            y: 10, // 10 pixels down from the top
            style: {
                fontSize: '10px',
                fontFamily: 'Verdana, sans-serif'
            }
        }

      ,
      exporting: {
        enabled: true,
        showTable: false,
        fileName: "line-chart"
      }
    } as any);
  }

  generarReporte3() {
    let date = new Date();
    const data: any = [];
    let monto: any = []
    monto = [];
    let rows: any = {}
    for (let i = 0; i < 12; i++) {
      monto.push(this.getRandomNumber(0, 1000))
    }
    rows ={
        name: 'SERVICIOS PÚBLICOS',
        data: monto,
    };
    data.push(rows)
    monto = [];
    for (let i = 0; i < 12; i++) {
      monto.push(this.getRandomNumber(0, 1000))
    }
    rows ={
        name: 'SERVICIO PASTORAL',
        data: monto,
    };
    data.push(rows)
    monto = [];
    for (let i = 0; i < 12; i++) {
      monto.push(this.getRandomNumber(0, 1000))
    }
    rows ={
        name: 'SERVICIO PARCIAL',
        data: monto,
    };
    data.push(rows)
    monto = [];
    for (let i = 0; i < 12; i++) {
      monto.push(this.getRandomNumber(0, 1000))
    }
    rows ={
        name: 'GASTO DE INMUEBLE',
        data: monto,
    };
    data.push(rows)
    monto = [];
    for (let i = 0; i < 12; i++) {
      monto.push(this.getRandomNumber(0, 1000))
    }
    rows ={
        name: 'MANTENIMIENTO INFRAESTRUCTURA',
        data: monto,
    };
    data.push(rows)
    console.log(JSON.stringify(data))

    const chart = Highcharts.chart('grafico-estadistico' as any, {
      chart: {
        type: 'column',
      },
      title: {
        text: 'Reporte de Egresos por Rubro',
      },
      credits: {
        enabled: false,
      },
      legend: {
        enabled: true,
      },
      yAxis: {
        min: 0,
        title: 'Meses',
      },
      xAxis: {
        type: 'category',
        labels: {
            rotation: 0,
            style: {
                fontSize: '11px',
                fontFamily: 'Verdana, sans-serif'
            }
        },
        categories: this.meses,
        crosshair: true
      },
      tooltip: {
        headerFormat: `<div>Mes: {point.key}</div>`,
        pointFormat: `<div>{series.name}: {point.y:.2f}</div>`,
        shared: true,
        useHTML: true,
      },
      plotOptions: {
        bar: {
          dataLabels: {
            enabled: true,
          },
        },
      },
      series: data,
        // {
        // name: 'Monto',
        // data ,
        dataLabels: {
            enabled: true,
            rotation: -90,
            color: '#FFFFFF',
            align: 'right',
            format: '{point.y:.2f}', // one decimal
            y: 10, // 10 pixels down from the top
            style: {
                fontSize: '10px',
                fontFamily: 'Verdana, sans-serif'
            }
        }

      ,
      exporting: {
        enabled: true,
        showTable: false,
        fileName: "line-chart"
      }
    } as any);
  }

  generarReporte4() {
    let date = new Date();
    const data: any = [];
    let monto: any = []
    monto = [];
    let rows: any = {}
    for (let i = 0; i < 12; i++) {
      monto.push(this.getRandomNumber(0, 1000))
    }
    rows ={
        name: 'VARONES',
        data: monto,
    };
    data.push(rows)
    monto = [];
    for (let i = 0; i < 12; i++) {
      monto.push(this.getRandomNumber(0, 1000))
    }
    rows ={
        name: 'MUJERES',
        data: monto,
    };
    data.push(rows)
    monto = [];
    for (let i = 0; i < 12; i++) {
      monto.push(this.getRandomNumber(0, 1000))
    }
    rows ={
        name: 'FAMILIA',
        data: monto,
    };
    data.push(rows)

    monto = [];
    for (let i = 0; i < 12; i++) {
      monto.push(this.getRandomNumber(0, 1000))
    }
    rows ={
        name: 'GENERAL',
        data: monto,
    };
    data.push(rows)

    console.log(JSON.stringify(data))

    const chart = Highcharts.chart('grafico-estadistico' as any, {
      chart: {
        type: 'column',
      },
      title: {
        text: 'Reporte de Ingresos por Sexo',
      },
      credits: {
        enabled: false,
      },
      legend: {
        enabled: true,
      },
      yAxis: {
        min: 0,
        title: 'Meses',
      },
      xAxis: {
        type: 'category',
        labels: {
            rotation: 0,
            style: {
                fontSize: '11px',
                fontFamily: 'Verdana, sans-serif'
            }
        },
        categories: this.meses,
        crosshair: true
      },
      tooltip: {
        headerFormat: `<div>Mes: {point.key}</div>`,
        pointFormat: `<div>{series.name}: {point.y:.2f}</div>`,
        shared: true,
        useHTML: true,
      },
      plotOptions: {
        bar: {
          dataLabels: {
            enabled: true,
          },
        },
      },
      series: data,
        // {
        // name: 'Monto',
        // data ,
        dataLabels: {
            enabled: true,
            rotation: -90,
            color: '#FFFFFF',
            align: 'right',
            format: '{point.y:.2f}', // one decimal
            y: 10, // 10 pixels down from the top
            style: {
                fontSize: '10px',
                fontFamily: 'Verdana, sans-serif'
            }
        }

      ,
      exporting: {
        enabled: true,
        showTable: false,
        fileName: "line-chart"
      }
    } as any);
  }

  generarReporte5() {
    let date = new Date();
    const data: any = [];
    let monto: any = []
    monto = [];
    let rows: any = {}
    for (let i = 0; i < 12; i++) {
      monto.push(this.getRandomNumber(0, 1000))
    }
    rows ={
        name: 'NIÑOS',
        data: monto,
    };
    data.push(rows)
    monto = [];
    for (let i = 0; i < 12; i++) {
      monto.push(this.getRandomNumber(0, 1000))
    }
    rows ={
        name: 'ADOLESCENTES',
        data: monto,
    };
    data.push(rows)
    monto = [];
    for (let i = 0; i < 12; i++) {
      monto.push(this.getRandomNumber(0, 1000))
    }
    rows ={
        name: 'JÓVENES',
        data: monto,
    };
    data.push(rows)
    monto = [];
    for (let i = 0; i < 12; i++) {
      monto.push(this.getRandomNumber(0, 1000))
    }
    rows ={
        name: 'ADULTOS',
        data: monto,
    };
    data.push(rows)
    monto = [];
    for (let i = 0; i < 12; i++) {
      monto.push(this.getRandomNumber(0, 1000))
    }
    rows ={
        name: 'FAMILIA',
        data: monto,
    };
    data.push(rows)

    monto = [];
    for (let i = 0; i < 12; i++) {
      monto.push(this.getRandomNumber(0, 1000))
    }
    rows ={
        name: 'GENERAL',
        data: monto,
    };
    data.push(rows)

    console.log(JSON.stringify(data))

    const chart = Highcharts.chart('grafico-estadistico' as any, {
      chart: {
        type: 'column',
      },
      title: {
        text: 'Reporte de Ingresos por Clasificación de Edad',
      },
      credits: {
        enabled: false,
      },
      legend: {
        enabled: true,
      },
      yAxis: {
        min: 0,
        title: 'Meses',
      },
      xAxis: {
        type: 'category',
        labels: {
            rotation: 0,
            style: {
                fontSize: '11px',
                fontFamily: 'Verdana, sans-serif'
            }
        },
        categories: this.meses,
        crosshair: true
      },
      tooltip: {
        headerFormat: `<div>Mes: {point.key}</div>`,
        pointFormat: `<div>{series.name}: {point.y:.2f}</div>`,
        shared: true,
        useHTML: true,
      },
      plotOptions: {
        bar: {
          dataLabels: {
            enabled: true,
          },
        },
      },
      series: data,
        // {
        // name: 'Monto',
        // data ,
        dataLabels: {
            enabled: true,
            rotation: -90,
            color: '#FFFFFF',
            align: 'right',
            format: '{point.y:.2f}', // one decimal
            y: 10, // 10 pixels down from the top
            style: {
                fontSize: '10px',
                fontFamily: 'Verdana, sans-serif'
            }
        }

      ,
      exporting: {
        enabled: true,
        showTable: false,
        fileName: "line-chart"
      }
    } as any);
  }

}
