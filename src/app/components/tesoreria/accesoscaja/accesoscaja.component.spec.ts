import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccesoscajaComponent } from './accesoscaja.component';

describe('AccesoscajaComponent', () => {
  let component: AccesoscajaComponent;
  let fixture: ComponentFixture<AccesoscajaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AccesoscajaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AccesoscajaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
