import { PaginationService } from './../../../services/pagination.service';
import { PageEvent } from '@angular/material/paginator';
import { ListService } from './../../../services/list.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { Caja } from 'src/app/models/caja';
import { Usuario } from 'src/app/models/usuario';
import { ControlesService } from 'src/app/services/controles.service';
import { NewService } from 'src/app/services/new.service';
import { SeguridadService } from 'src/app/services/seguridad.service';
import { SidenavService } from 'src/app/services/sidenav.service';



@Component({
  selector: 'app-accesoscaja',
  templateUrl: './accesoscaja.component.html',
  styleUrls: ['./accesoscaja.component.sass']
})
export class AccesoscajaComponent implements OnInit {
  dataGlobal: Caja[] = [];
  collectionSize = 0;
  pageEvent: PageEvent = new PageEvent;
  public dataUser: Usuario[] = []
  public banperm: boolean = false
  public caja: string = 'Caja 01';
  myControl = new FormControl();
  title: string = 'Caja y accesos';
  route: string = 'accesoscaja'
  modulo: string = 'TESORERIA';
  @ViewChild('drawer') sideNav: any;
  constructor(
    public nvoservc: NewService,
    public sidenavservc: SidenavService,
    public lstserv: ListService,
    public ctrlserv: ControlesService,
    public segserv:SeguridadService,
    private router: Router,
    public pageService: PaginationService,
  ) {
     this.segserv.disload = false;
     this.pageEvent = this.pageService.inicializar()
  }

  ngOnInit(): void {
    this.listar_cajas();
  }

  ngAfterViewInit(): any {
    this.sidenavservc.abrirmenu(this.sideNav);
    this.nvoservc.nuevo = () => {
      this.nvoservc.nuevacaja();
    };

    this.lstserv.listar = () => {
      this.listar_cajas();
    }
  }

  permisodiv($state: boolean){
    this.banperm = $state;
  }

  listar_cajas(){

    this.lstserv.listado('list-caja', '?activo=-1').then(data => {
    console.log(JSON.stringify(data))
    if(data.length != this.dataGlobal.length){
      this.dataGlobal = data
    }

    this.pageService.dataSource = this.dataGlobal
    console.log(JSON.stringify(this.pageService.dataSource ))
    this.pageService.coleccionsize = this.pageService.dataSource.length
    this.pageEvent.length = data.length
    this.collectionSize = this.pageService.dataSource.length
    this.pageService.actualizaTabla2(this.dataGlobal, this.pageEvent);
  })
}

  getTotal($tipo: number){
    return this.dataGlobal
    .reduce(
      (sum, value) =>
        typeof parseFloat(value.saldo) === 'number'
          ? sum + parseFloat(value.saldo)
          : sum, 0
    )
    .toFixed(2);
  }

}
