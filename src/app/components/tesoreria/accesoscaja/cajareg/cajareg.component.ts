import { AppDateAdapter, APP_DATE_FORMATS } from 'src/app/helpers/format-datepicker';
import { DateAdapter, MAT_DATE_FORMATS } from '@angular/material/core';
import { RegisterService } from 'src/app/services/register.service';
import { Component, Inject, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConfiguracionService } from 'src/app/services/configuracion.service';

@Component({
  selector: 'app-cajareg',
  templateUrl: './cajareg.component.html',
  styleUrls: ['./cajareg.component.sass'],
  providers: [
    {provide: DateAdapter, useClass: AppDateAdapter},
    {provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS}
  ]
})
export class CajaregComponent implements OnInit {
  myControl = new FormControl();
  aperturatotal: any = "0.00"
  public today = new Date()
  public minDate = new Date()
  date: any;
  constructor(
    public configserv: ConfiguracionService,
    public regserv: RegisterService,
    public dialogRef: MatDialogRef<CajaregComponent>,
    @Inject(MAT_DIALOG_DATA) public data: {title: 'Registro de caja' }
  ) {
    this.date = new FormControl(new Date());
  }

  ngOnInit(): void {
  }

}
