import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CajaregComponent } from './cajareg.component';

describe('CajaregComponent', () => {
  let component: CajaregComponent;
  let fixture: ComponentFixture<CajaregComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CajaregComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CajaregComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
