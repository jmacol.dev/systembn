import { configuracioninforme } from './../../../../models/configuracioninforme';
import { ListService } from 'src/app/services/list.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConfiguracionService } from './../../../../services/configuracion.service';
import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'app-configuracion-informe',
  templateUrl: './configuracion-informe.component.html',
  styleUrls: ['./configuracion-informe.component.sass']
})
export class ConfiguracionInformeComponent implements OnInit {
  dataIngConfig: configuracioninforme[] = []
  dataEgrConfig: configuracioninforme[] = []
  constructor(public configserv: ConfiguracionService,

    public listserv: ListService,
    public dialogRef: MatDialogRef<ConfiguracionInformeComponent>,
    @Inject(MAT_DIALOG_DATA) public data: {title: 'Configuración de Informe' }) { }

  ngOnInit(): void {
    this.listserv.listado('list-configinforme', '').then(data => {
      this.dataEgrConfig = data.filter((x: configuracioninforme) => x.tipo == 0);
      this.dataIngConfig = data.filter((x: configuracioninforme) => x.tipo == 1);
      console.log(this.dataIngConfig)
      console.log(this.dataEgrConfig)
    })
  }

  guardar() {
    console.log(this.dataIngConfig)
    console.log(this.dataEgrConfig)
  }

}
