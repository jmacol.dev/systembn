import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfiguracionInformeComponent } from './configuracion-informe.component';

describe('ConfiguracionInformeComponent', () => {
  let component: ConfiguracionInformeComponent;
  let fixture: ComponentFixture<ConfiguracionInformeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConfiguracionInformeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfiguracionInformeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
