import { SidenavService } from './../../../services/sidenav.service';
import { NewService } from './../../../services/new.service';
import { ListService } from './../../../services/list.service';
import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-informe-mensual',
  templateUrl: './informe-mensual.component.html',
  styleUrls: ['./informe-mensual.component.sass']
})
export class InformeMensualComponent implements OnInit {

  title: string = 'Informe Mensual';
  route: string = 'informe-mensual'
  modulo: string = 'TESORERIA';

  @ViewChild('drawer') sideNav: any;
  constructor(
    private sidenavservc: SidenavService,
    private nvoservc: NewService,
    public lstserv: ListService
  ) { }

  ngOnInit(): void {
  }

  ngAfterViewInit(): any {
    this.sidenavservc.abrirmenu(this.sideNav);
    this.nvoservc.nuevo = () => {
      this.nvoservc.configuracionInforme();
    };
    this.lstserv.listar = () =>{

    }

}

}
