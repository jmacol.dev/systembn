import { diezmoofrenda } from './../../../../models/diezmosofrenda';
import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { ConfiguracionService } from 'src/app/services/configuracion.service';
import { ControlesService } from 'src/app/services/controles.service';
import { ListService } from 'src/app/services/list.service';
import { NewmodalService  } from 'src/app/services/newmodal.service';
import { RegisterService } from 'src/app/services/register.service';
import { ValidateService } from 'src/app/services/validate.service';


@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.sass']
})
export class RegistroComponent implements OnInit {
  @Output() openList = new EventEmitter();
  myControl = new FormControl();
  options: [] | undefined;
  filteredOptions: Observable<any> | undefined;
  constructor(
    public dialogRef: MatDialogRef<RegistroComponent>,
    public configserv: ConfiguracionService,
    public regserv: RegisterService,
    public lstserv: ListService,
    public nvomodservic: NewmodalService,
    public valserv: ValidateService,
    @Inject(MAT_DIALOG_DATA) public data: { title: 'Registro de ingresos', id: '', data: diezmoofrenda, listar: false }
  ) {
    if (this.data.id != '') {
      this.regserv.diezmoofrenda_reg = this.data.data;
      this.regserv.movimientocaja_reg.responsable = this.data.data.idaportante
      this.regserv.diezmoofrenda_reg.idaportante = this.data.data.idaportante
      console.log(JSON.stringify(this.data.data))
      console.log(this.regserv.movimientocaja_reg.responsable)
    } else {
      this.regserv.diezmoofrenda_reg = this.regserv.empty_diezmoofrenda()
    }
  }

  ngOnInit(): void {

  }

  getMonto(){
    return  (parseFloat(this.regserv.diezmoofrenda_reg.diezmo) +
    parseFloat(this.regserv.diezmoofrenda_reg.ofrenda) +
    parseFloat(this.regserv.diezmoofrenda_reg.misericordia) +
    parseFloat(this.regserv.diezmoofrenda_reg.misiones) +
    parseFloat(this.regserv.diezmoofrenda_reg.protemplo)).toFixed(2);
  }

  guardaringreso(e: any){
    if(this.valserv.validatediezmoofrenda(this.getMonto(), 'Tesorería', this.data.title)){
      this.regserv.diezmoofrenda_reg.idaportante = this.regserv.movimientocaja_reg.responsable
      this.regserv.diezmoofrenda_reg.idgruporecaudacion = this.regserv.gruporecaudacion_reg.id
      this.regserv.diezmoofrenda_reg.total = this.getMonto()
      this.regserv.registro('Registro de diezmos y ofrendas', 'register-diezmoofrenda', {
        id: this.regserv.diezmoofrenda_reg.id,
        diezmo: this.regserv.diezmoofrenda_reg.diezmo,
        ofrenda: this.regserv.diezmoofrenda_reg.ofrenda,
        misiones: this.regserv.diezmoofrenda_reg.misiones,
        misericordia: this.regserv.diezmoofrenda_reg.misericordia,
        protemplo: this.regserv.diezmoofrenda_reg.protemplo,
        total: this.regserv.diezmoofrenda_reg.total,
        idgruporecaudacion: this.regserv.diezmoofrenda_reg.idgruporecaudacion,
        idaportante: this.regserv.diezmoofrenda_reg.idaportante,
        aportefamiliar: this.regserv.diezmoofrenda_reg.aportefamiliar
      }).then(value=>{
          this.lstserv.listar(this.openList.emit(e))
          this.configserv.onNoClickDialog(this.dialogRef)
      })
    }
  }

}
