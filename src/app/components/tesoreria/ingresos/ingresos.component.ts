import { NotificacionesService } from './../../../services/notificaciones.service';
import { AppDateAdapter, APP_DATE_FORMATS } from './../../../helpers/format-datepicker';
import { DateAdapter, MAT_DATE_FORMATS } from '@angular/material/core';
import { ExportarexcelService } from './../../../services/exportarexcel.service';
import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { diezmoofrenda } from 'src/app/models/diezmosofrenda';
import { ConfiguracionService } from 'src/app/services/configuracion.service';
import { ControlesService } from 'src/app/services/controles.service';
import { ListService } from 'src/app/services/list.service';
import { NewService } from 'src/app/services/new.service';
import { NewmodalService } from 'src/app/services/newmodal.service';
import { PaginationService } from 'src/app/services/pagination.service';
import { RegisterService } from 'src/app/services/register.service';
import { SeguridadService } from 'src/app/services/seguridad.service';
import { SidenavService } from 'src/app/services/sidenav.service';
import { ValidateService } from 'src/app/services/validate.service';
import Swal from 'sweetalert2';
import { RegistroComponent } from './registro/registro.component';
import { jsPDF } from 'jspdf';
import autoTable from 'jspdf-autotable'
@Component({
  selector: 'app-ingresos',
  templateUrl: './ingresos.component.html',
  styleUrls: ['./ingresos.component.sass'],
  providers: [
    {provide: DateAdapter, useClass: AppDateAdapter},
    {provide: MAT_DATE_FORMATS , useValue: APP_DATE_FORMATS  }
  ]
})
export class IngresosComponent implements OnInit {
  modulo: string = 'TESORERIA';
  title: string = 'Registro de diezmos y ofrendas';
  route: string = 'diezmosofrendas'
  isMobile = false;
  myControl = new FormControl();
  options: [] | undefined;
  filteredOptions: Observable<any> | undefined;
  @ViewChild('drawer') sideNav: any;
  bantest:boolean = true;
  displayedColumns: string[] = [
    'name',
    'diezmo',
    'ofrenda',
    'misiones',
    'misericordia',
    'protemplo',
    'total'
  ];

  dataGlobal: diezmoofrenda[] = [];
  collectionSize = 0;
  pageEvent: PageEvent = new PageEvent;
  constructor(
    private sidenavservc: SidenavService,
    private nvoservc: NewService,
    public pageService: PaginationService,
    public dialog: MatDialog,
    public regserv: RegisterService,
    public lstserv: ListService,
    public valserv: ValidateService,
    public ctrlserv: ControlesService,
    public mdlserv: NewmodalService,
    public configserv: ConfiguracionService,
    public segserv:SeguridadService,
    private router: Router,
    private excelserv: ExportarexcelService
  ) {
      this.pageEvent = this.pageService.inicializar()
  }

  ngOnInit(): void {
    console.log('cargando componenete');
  }

  //
ngAfterViewInit(): any {
  this.sidenavservc.abrirmenu(this.sideNav);
  this.nvoservc.nuevo = () => {
    this.nvoservc.nuevoingreso('',this.regserv.empty_diezmoofrenda(), true);
  };

  this.lstserv.listar = (e) => {
    console.log(e)
    if(e=='listardo') this.dataGlobal = [];
    this.listar_diezmosofrendas();
    this.regserv.movimientocaja_reg.autorizacion = ''
    this.regserv.movimientocaja_reg.responsable = ''
      this.ctrlserv.disctrolmbr = (this.regserv.gruporecaudacion_reg.fechacierre != null ? false : true);
  }

  this.lstserv.excel = () => {
    console.log("exportar")
   this.exportExcel()
  }

   this.lstserv.pdf = () => {
    console.log("exportar pdf")
   this.exportPdf()
  }

}

  dataForExcel:any[] = [];
  empPerformance:any[] = []
  exportExcel(){
    if(this.dataGlobal.length>0){
      this.empPerformance = [];
      this.dataGlobal.forEach((item) => {
        this.empPerformance.push({
          'NOMBRE': item.aportante, "DIEZMOS": this.segserv.moneda + parseFloat(item.diezmo).toFixed(2), "OFRENDAS": this.segserv.moneda + parseFloat(item.ofrenda).toFixed(2), "MISIONES": this.segserv.moneda + parseFloat(item.misiones).toFixed(2), "MISERICORDIA": this.segserv.moneda + parseFloat(item.misericordia).toFixed(2), "PRO_TEMPLO": this.segserv.moneda + parseFloat(item.protemplo).toFixed(2), "TOTAL": this.segserv.moneda + parseFloat(item.total).toFixed(2)
        });
      });
      this.empPerformance.forEach((row: any) => {
        this.dataForExcel.push(Object.values(row))
      })
      let d = new Date();
      let date = d.getDate().toString().padStart(2, "0") + '-' + (d.getMonth() + 1).toString().padStart(2, "0") + '-' + d.getFullYear();
      let fecha = this.configserv.datepipe.transform(this.regserv.movimientocaja_reg.fecha, "dd-MM-YYYY")
      let reportData = {
        title: 'Diezmos y Ofrendas :: ' + fecha,
        data: this.dataForExcel,
        headers: Object.keys(this.empPerformance[0]),
        description: "Diezmos y Ofrendas | " + this.regserv.gruporecaudacion_reg.caja + "|" +  this.regserv.gruporecaudacion_reg.codigo
      }

      this.excelserv.exportExcelDiezmosOfrendas(reportData);
    } else {
      this.valserv.validaban = false;
      this.valserv.showError('No hay registro de diezmos y ofrendas.', this.modulo + ' - ' + this.title)
    }
  }


  exportPdf() {
     let fecha = this.configserv.datepipe.transform(this.regserv.movimientocaja_reg.fecha, "dd-MM-YYYY")
  const lastPoint = { x: 0, y: 0 };
  const doc = new jsPDF('l', 'mm', [297, 210]);
  const header = 'DIEZMOS Y OFRENDAS :: ' + fecha?.toString() + ' | ' + this.regserv.gruporecaudacion_reg.caja + " |"  +  this.regserv.gruporecaudacion_reg.codigo
  const pageWidth = doc.internal.pageSize.getWidth();
  doc.setFontSize(10);
  doc.setFont("Helvetica", "bold");
  const headerWidth = doc.getTextDimensions(header).w;
  doc.text(header, (pageWidth - headerWidth) / 2, 15);

  var col = ['N°','NOMBRE', 'DIEZMOS', 'OFRENDAS', 'MISIONES', 'MISERICORDIA', 'PRO-TEMPLO', 'TOTAL'];
  var rows:any = [];

   this.dataGlobal.forEach((item, index) => {
  // tslint:disable-next-line:max-line-length
    const temp = [(index + 1), item.aportante, item.diezmo, item.ofrenda, item.misiones, item.misericordia, item.protemplo, item.total];
    rows.push(temp);

  });



  autoTable(doc, {
    head: [col],
    body:
      rows
    , styles: { fontSize: 7, overflow: 'linebreak' }, startY: 20, columnStyles: { 0: { cellWidth: 10 }, 2: { halign: 'right' }, 3: { halign: 'right' }, 4: { halign: 'right' }, 5: { halign: 'right' }, 6: { halign: 'right' }, 7: { halign: 'right' } }
  })

  const footer = 'Sistema Integral - TESORERÍA';
  doc.setFontSize(7);
  doc.setTextColor('#cccccc');
  const footerWidth = doc.getTextDimensions(footer).w;
  doc.text(footer, (lastPoint.x - footerWidth), lastPoint.y + 30);
  doc.save('DiezmosOfrendas.pdf');
  }

listar_diezmosofrendas(){

  this.lstserv.listado('list-diezmoofrenda','?grupo='+this.regserv.gruporecaudacion_reg.id+'&caja='+this.regserv.caja_reg.id+'&fecha='+
  this.configserv.datepipe.transform(this.regserv.movimientocaja_reg.fecha, "YYYY-MM-dd")).then(data=>{
    if(data.length != this.dataGlobal.length){
      this.dataGlobal = data
    }
    this.pageService.coleccionsize = this.pageService.dataSource.length
    this. pageService.dataSource = data
    this.pageEvent.length = data.length
    this.collectionSize = this.pageService.dataSource.length
    this.pageService.actualizaTabla2(data, this.pageEvent);
  })
}



  guardargrupo(){

    // autorizacion: primer testigo
    // responsable: segundo testigo
    if(this.valserv.validarcierregrupo(this.lstserv.getTotal(0, this.dataGlobal), this.regserv.movimientocaja_reg.responsable, this.regserv.movimientocaja_reg.autorizacion, this.regserv.caja_reg.id, this.modulo, this.title)){
    Swal.fire({
      title: 'Grupo de Recaudación',
      text: '¿Está seguro dar por cerrado este grupo de recaudación? Una vez finalizado no podrás agregar más registros.',
      icon: 'question',
      showCancelButton: true,
      confirmButtonText: 'Aceptar',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.value) {
        this.regserv.gruporecaudacion_reg.primertestigo = this.regserv.movimientocaja_reg.responsable
        this.regserv.gruporecaudacion_reg.segundotestigo = this.regserv.movimientocaja_reg.autorizacion
        this.regserv.gruporecaudacion_reg.monto = this.lstserv.getTotal(0, this.dataGlobal)
        let request ={
          id: this.regserv.gruporecaudacion_reg.id ,
          fecha: this.regserv.gruporecaudacion_reg.fecha ,
          primertestigo: this.regserv.gruporecaudacion_reg.primertestigo ,
          segundotestigo: this.regserv.gruporecaudacion_reg.segundotestigo ,
          monto: this.regserv.gruporecaudacion_reg.monto ,
          idcaja: this.regserv.gruporecaudacion_reg.idcaja ,
          codigo: this.regserv.gruporecaudacion_reg.codigo,
          mtodiemzo:this.lstserv.getTotal(1, this.dataGlobal),
          mtoofrenda:this.lstserv.getTotal(2, this.dataGlobal),
          mtomisiones:this.lstserv.getTotal(3, this.dataGlobal),
          mtomisericordia:this.lstserv.getTotal(4, this.dataGlobal),
          mtoprotemplo:this.lstserv.getTotal(5, this.dataGlobal)
        }

        this.regserv.actualizar('Cierre de grupo de recaudación', 'update-gruporecaudacion', request).then(value=>{
          if(value){
            this.ctrlserv.listargruporecaudacion(2)
          }
        })
      }})
    }
  }

  eliminar_diezmo($diezmo: diezmoofrenda){
    this.mdlserv.nuevoModalEliminar('Eliminar registro de diezmos y ofrendas', 'Al eliminar este registro ya no aparecerá en el grupo de recadaución.', $diezmo.id, 'delete-diezmoofrenda');
  }

  editar_diezmo($diezmo: diezmoofrenda) {
    this.nvoservc.nuevoingreso($diezmo.id, $diezmo, true);
  }
}
