import { NotificacionesService } from './../../../services/notificaciones.service';
import { ValidateService } from 'src/app/services/validate.service';
import { ExportarexcelService } from './../../../services/exportarexcel.service';
import { Component, EventEmitter, OnInit, Output, ViewChild, AfterViewInit, OnDestroy } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { movimientocaja } from 'src/app/models/movimientocaja';
import { ConfiguracionService } from 'src/app/services/configuracion.service';
import { ControlesService } from 'src/app/services/controles.service';
import { ListService } from 'src/app/services/list.service';
import { NewService } from 'src/app/services/new.service';
import { NewmodalService } from 'src/app/services/newmodal.service';
import { PaginationService } from 'src/app/services/pagination.service';
import { RegisterService } from 'src/app/services/register.service';
import { SeguridadService } from 'src/app/services/seguridad.service';
import { SidenavService } from 'src/app/services/sidenav.service';
import { EgresosComponent } from './../egresos/egresos.component';
import { jsPDF } from 'jspdf';
import autoTable from 'jspdf-autotable'
@Component({
  selector: 'app-caja',
  templateUrl: './caja.component.html',
  styleUrls: ['./caja.component.sass']
})

export class CajaComponent implements OnInit {
  // navigationSubscription;
  modulo: string = 'TESORERIA';
  title: string = 'Movimientos de caja';
  route: string = 'caja'
  myControl = new FormControl();
  options: [] | undefined;
  filteredOptions: Observable<any> | undefined;
  @ViewChild('drawer') sideNav: any;

  displayedColumns: string[] = [
    'name',
    'diezmo',
    'ofrenda',
    'misiones',
    'misericordia',
    'protemplo',
    'total'
  ];
  dataglobal: movimientocaja[] = [];
  collectionSize = 0;
  pageEvent: PageEvent = new PageEvent;
  constructor(
    // public router: Router,
    private sidenavservc: SidenavService,
    private nvoservc: NewService,
    public lstserv: ListService,
    public configserv: ConfiguracionService,
    public pageService: PaginationService,
    public dialog: MatDialog,
    public regserv: RegisterService,
    public mdlserv: NewmodalService,
    public ctrslserv: ControlesService,
    public segserv:SeguridadService,
    private router: Router,
    private excelserv: ExportarexcelService,
    public valserv: ValidateService
  ) {
    this.pageEvent = this.pageService.inicializar()
  }



  ngOnInit(): void {
    console.log('cargando componente');
  }

  eliminar_movcaja($movcaja: movimientocaja){
    this.mdlserv.nuevoModalEliminar('Eliminar movimiento caja', 'Al eliminar este registro ya no aparecerá en los registros de caja y actualizará el saldo', $movcaja.id, 'delete-movimientocaja');

  }
 verdetalle($movcaja: movimientocaja){
  this.mdlserv.nuevoModalDetalleGrupo("Detalle de Grupo de Recaudación::"+$movcaja.gruporecaudacion + ' / CAJA: ' + $movcaja.caja
  +' / FECHA: '+   this.configserv.datepipe.transform($movcaja.fecha, "dd-MM-YYYY")
  , $movcaja.idgruporecaudacion);
 }
ngAfterViewInit(): any {
  this.sidenavservc.abrirmenu(this.sideNav);
  this.nvoservc.nuevo = () => {
    this.nvoservc.nuevoegreso();
  };
  this.lstserv.listar = () =>{

    this.listar_movimientoscaja()
    this.ctrslserv.getcajasaldo();
  }

  this.lstserv.excel = () => {
    console.log("exportar")
   this.exportExcel()
  }

  this.lstserv.pdf = () => {
    console.log("exportar pdf")
   this.exportPdf()
  }

}

dataForExcel:any[] = [];
empPerformance:any[] = []
exportExcel(){
    if(this.dataglobal.length>0){
      this.empPerformance = [];
      this.dataglobal.forEach((item) => {
        this.empPerformance.push({
          'FECHA': this.configserv.datepipe.transform(item.fecha, "dd-MM-yyyy"), "INDICADOR": item.indicador == 1 ? 'INGRESO' : 'EGRESO', "TIPO GASTO": item.tipomovimiento.toUpperCase(), "DESCRIPCION": item.detallemovimiento.toUpperCase(), "CAJA": item.caja, "MONTO":  (item.monto * item.indicador).toFixed(2), "RESPONSABLE": item.nombreresponsable
          , "COMENTARIO": item.comentario
        });
      });
      this.empPerformance.forEach((row: any) => {
        this.dataForExcel.push(Object.values(row))
      })
      let d = new Date();
      let date = d.getDate().toString().padStart(2, "0") + '-' + (d.getMonth() + 1).toString().padStart(2, "0") + '-' + d.getFullYear();
      let fechamax = this.configserv.datepipe.transform(this.lstserv.fechamax, "dd-MM-YYYY")
      let fechamin = this.configserv.datepipe.transform(this.lstserv.fechamin, "dd-MM-YYYY")
      let reportData = {
        title: 'Movimientos de Caja :: DEL ' + fechamin?.toString() + ' AL ' + fechamax?.toString(),
        data: this.dataForExcel,
        headers: Object.keys(this.empPerformance[0]),
        description: "Listado de movimientos de caja"
      }

      this.excelserv.exportExcelMovimientosCaja(reportData);
    } else {
      this.valserv.validaban = false;
      this.valserv.showError('No hay registro de movimientos de caja.', this.modulo + ' - ' + this.title)
    }
}

  exportPdf() {
    let fechamax = this.configserv.datepipe.transform(this.lstserv.fechamax, "dd-MM-YYYY")
      let fechamin = this.configserv.datepipe.transform(this.lstserv.fechamin, "dd-MM-YYYY")
  const lastPoint = { x: 0, y: 0 };
  const doc = new jsPDF('l', 'mm', [297, 210]);
  const header = 'MOVIMIENTOS DE CAJA :: ' + fechamin?.toString() + ' al ' + fechamax?.toString()
  const pageWidth = doc.internal.pageSize.getWidth();
    doc.setFontSize(10);
    doc.setFont("Helvetica", "bold");
  const headerWidth = doc.getTextDimensions(header).w;
  doc.text(header, (pageWidth - headerWidth) / 2, 15);

  var col = ['N°','FECHA', 'INDICADOR', 'TIPO GASTO', 'DESCRIPCIÓN', 'CAJA', 'MONTO', 'RESPONSABLE', 'COMENTARIO'];
  var rows:any = [];

   this.dataglobal.forEach((item, index) => {
  // tslint:disable-next-line:max-line-length
    const temp = [(index + 1),this.configserv.datepipe.transform(item.fecha, "dd-MM-yyyy"), item.indicador == 1 ? 'INGRESO' : 'EGRESO', item.tipomovimiento.toUpperCase(), item.detallemovimiento.toUpperCase(), item.caja, (item.monto * item.indicador).toFixed(2), item.nombreresponsable
          , item.comentario];
    rows.push(temp);

  });



  autoTable(doc, {
    head: [col],
    body:
      rows
    , styles : {fontSize:7, overflow: 'linebreak'}, startY: 20, columnStyles: { 0:{ cellWidth: 10}, 1: { halign: 'center', cellWidth: 17}, 2: {cellWidth: 25}, 3: {cellWidth: 35}, 4: {cellWidth: 45},5: {cellWidth: 17}, 6: { halign: 'right', cellWidth: 20}},
  })

  const footer = 'Sistema Integral - TESORERÍA';
  doc.setFontSize(7);
  doc.setTextColor('#cccccc');
  const footerWidth = doc.getTextDimensions(footer).w;
  doc.text(footer, (lastPoint.x - footerWidth), lastPoint.y + 5);
  doc.save('MovimientosCaja.pdf');
}



listar_movimientoscaja(){
  let fechamax = this.configserv.datepipe.transform(this.lstserv.fechamax, "YYYY-MM-dd")
  let fechamin = this.configserv.datepipe.transform(this.lstserv.fechamin, "YYYY-MM-dd")

  this.lstserv.listado('list-movimientocaja','?idcaja='+this.regserv.caja_reg.id+'&fechamax='+fechamax+'&fechamin='+fechamin).then(data=>{
    if(data.length != this.dataglobal.length){
      this.dataglobal = data
      this.pageService.coleccionsize = this.pageService.dataSource.length
      this. pageService.dataSource = data
      this.collectionSize = this.pageService.dataSource.length
      this.pageEvent.length = data.length
      this.pageService.actualizaTabla2(data, this.pageEvent);
    }
  })
}
  getTotal($tipo: number){
    return this.dataglobal
    .reduce(
      (sum, value) =>
        typeof (parseFloat(value.monto) * value.indicador) === 'number'
          ? sum + (parseFloat(value.monto) * value.indicador)
          : sum, 0
    )
    .toFixed(2);
  }


}
