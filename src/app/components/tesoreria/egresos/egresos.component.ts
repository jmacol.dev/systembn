import { SeguridadService } from 'src/app/services/seguridad.service';
import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { DateAdapter, MAT_DATE_FORMATS } from '@angular/material/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { AppDateAdapter, APP_DATE_FORMATS } from 'src/app/helpers/format-datepicker';
import { CategoriaCbo } from 'src/app/models/categoria';
import { ConfiguracionService } from 'src/app/services/configuracion.service';
import { ControlesService } from 'src/app/services/controles.service';
import { ListService } from 'src/app/services/list.service';
import { NewService } from 'src/app/services/new.service';
import { NewmodalService } from 'src/app/services/newmodal.service';
import { RegisterService } from 'src/app/services/register.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-egresos',
  templateUrl: './egresos.component.html',
  styleUrls: ['./egresos.component.sass'],
  providers: [
    {provide: DateAdapter, useClass: AppDateAdapter},
    {provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS}
  ]
})
export class EgresosComponent implements OnInit {
  @Output() openList2 = new EventEmitter();
  myControl = new FormControl();
  date: any;
  datacbo: CategoriaCbo[] = []
  tipogastoslst: CategoriaCbo[] = []
  descripciongastolst: CategoriaCbo[] = []
  destinolst: CategoriaCbo[] = []
  public today = new Date()
  public minDate = new Date()
  public tipogasto: string = ''
  public descgasto: string = ''
  constructor(
    public dialogRef: MatDialogRef<EgresosComponent>,
    public nvomodservic: NewmodalService,
    public configserv: ConfiguracionService,
    public segserv: SeguridadService,
    public regserv: RegisterService,
    public ctrlserv: ControlesService,
    public lstserv: ListService,
    @Inject(MAT_DIALOG_DATA) public data: {title: 'Registro de ingresos', egreso: true },

  ) {
    this.initialiseInvites();
   }


   initialiseInvites() {
    // Set default values and re-fetch any data you need.
    this.date = new FormControl(new Date());
      this.minDate.setDate(this.minDate.getDate() - 7)
      let route: string = (this.data.egreso ? 'list-egresos' : 'list-ingresos')
      this.lstserv.listado(route, '').then(dataaux=>{
        this.datacbo = dataaux;
        this.tipogastoslst = this.datacbo.filter(item => item.superior == null)

        this.lstserv.listado('list-rubroscaja', '').then(data =>{
          this.destinolst = data;
        })
      })
      this.regserv.movimientocaja_reg =  this.regserv.empty_movimientocaja()

      this.ctrlserv.saldorubro = 0.0.toFixed(2)
  }

  ngOnInit(): void {
  }

  // ngAfterViewInit(): any {
  //   this.lstserv.listar = () =>{
  //     this.ctrlserv.getcajasaldo()
  //     this.selrubro()
  //   }
  // }

  selrubro(){
    let tipo = this.destinolst.filter(item=>item.id == this.regserv.movimientocaja_reg.destino)[0].descripcion
    switch(tipo.toUpperCase()){
      case 'DIEZMOS': this.ctrlserv.saldorubro = this.ctrlserv.cajasaldo.diezmos; break;
      case 'OFRENDAS': this.ctrlserv.saldorubro = this.ctrlserv.cajasaldo.ofrendas; break;
      case 'MISIONES': this.ctrlserv.saldorubro = this.ctrlserv.cajasaldo.misiones; break;
      case 'MISERICORDIA': this.ctrlserv.saldorubro = this.ctrlserv.cajasaldo.misericordia; break;
      case 'PRO-TEMPLO': this.ctrlserv.saldorubro = this.ctrlserv.cajasaldo.protemplo; break;
    }
  }

  listardetalle($item: CategoriaCbo){
    this.descripciongastolst = this.datacbo.filter(item => item.superior == $item.id)
    this.descgasto = ''
  }

  guardaregresos(e: any){
    if(this.data.egreso && parseFloat(this.regserv.movimientocaja_reg.monto) > parseFloat(this.ctrlserv.saldorubro)){
        Swal.fire("Movimientos de Caja", "No se puede registrar porque el monto ingresado es mayor al saldo.", "info")
    }else{
      this.regserv.movimientocaja_reg.indicador = this.data.egreso ? -1 : 1
      this.regserv.movimientocaja_reg.idCaja = this.regserv.caja_reg.id
      let tipo = this.destinolst.filter(item=>item.id == this.regserv.movimientocaja_reg.destino)[0].descripcion
      let request ={
        fecha : this.configserv.datepipe.transform(this.regserv.movimientocaja_reg.fecha, "YYYY-MM-dd"),
        indicador : this.regserv.movimientocaja_reg.indicador,
        comentario : this.regserv.movimientocaja_reg.comentario,
        activo : this.regserv.movimientocaja_reg.activo,
        idCaja : this.regserv.movimientocaja_reg.idCaja,
        idtipomovimiento : this.regserv.movimientocaja_reg.idtipomovimiento,
        responsable : this.regserv.movimientocaja_reg.responsable,
        autorizacion : this.data.egreso ? this.regserv.movimientocaja_reg.autorizacion : null,
        monto : this.regserv.movimientocaja_reg.monto,
        idtipogasto : this.regserv.movimientocaja_reg.idtipogasto,
        destino: this.regserv.movimientocaja_reg.destino,
        mtodiemzo: tipo == 'DIEZMOS' ? this.regserv.movimientocaja_reg.monto : 0,
        mtoofrenda:tipo == 'OFRENDAS' ? this.regserv.movimientocaja_reg.monto : 0,
        mtomisiones: tipo == 'MISIONES' ? this.regserv.movimientocaja_reg.monto : 0,
        mtomisericordia:tipo == 'MISERICORDIA' ? this.regserv.movimientocaja_reg.monto : 0,
        mtoprotemplo: tipo == 'PRO-TEMPLO' ? this.regserv.movimientocaja_reg.monto : 0,
        aportefamiliar: this.data.egreso ? 0 : this.regserv.movimientocaja_reg.aportefamiliar
      }

      this.regserv.registro('Movimientos de Caja', 'insert-movimientocaja', request).then(value=>{
        if(value){
          this.lstserv.listar(this.openList2.emit(e))
          this.configserv.onNoClickDialog(this.dialogRef);
        }
      })
    }
  }
}
