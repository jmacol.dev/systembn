import { NotificacionesService } from './../../../services/notificaciones.service';
import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { DateAdapter, MAT_DATE_FORMATS } from '@angular/material/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AppDateAdapter, APP_DATE_FORMATS } from 'src/app/helpers/format-datepicker';
import { CategoriaCbo } from 'src/app/models/categoria';
import { ConfiguracionService } from 'src/app/services/configuracion.service';
import { ControlesService } from 'src/app/services/controles.service';
import { ListService } from 'src/app/services/list.service';
import { NewService } from 'src/app/services/new.service';
import { RegisterService } from 'src/app/services/register.service';
import { SeguridadService } from 'src/app/services/seguridad.service';
import { ValidateService } from 'src/app/services/validate.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-transfrubros',
  templateUrl: './transfrubros.component.html',
  styleUrls: ['./transfrubros.component.sass'],
  providers: [
    {provide: DateAdapter, useClass: AppDateAdapter},
    {provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS}
  ]
})
export class TransfrubrosComponent implements OnInit {
  @Output() openList = new EventEmitter();
  public destinolst: CategoriaCbo[] = []
  public destinocargolst: CategoriaCbo[] = []
  date: any;
  public today = new Date()
  public minDate = new Date()
  titulo = 'Tesorería - Transferencias'
  constructor(
    public configserv: ConfiguracionService,
    public ctrlserv: ControlesService,
    public lstserv: ListService,
    public regserv: RegisterService,
    public valserv:ValidateService,
    public segserv: SeguridadService,
    public dialogRef: MatDialogRef<TransfrubrosComponent>,
    @Inject(MAT_DIALOG_DATA) public data: {title: 'Transferencias Rubros', abono: '' },
  ) {
    this.date = new FormControl(new Date());
    this.minDate.setDate(this.minDate.getDate() - this.ctrlserv.minfechadias)
    this.regserv.transferenciarubro_reg.idcaja = this.regserv.caja_reg.id
    this.regserv.transferenciarubro_reg.idrubrocargo = ''
    this.ctrlserv.saldorubrotransf = 0.0.toFixed(2)
  }

  ngOnInit(): void {
    this.lstserv.listado('list-rubroscaja', '').then(data =>{
      this.destinolst = data;
      this.destinocargolst = this.destinolst.filter(item=> item.id != this.data.abono)
      this.regserv.transferenciarubro_reg.idrubroabona = this.data.abono
    })
  }

  selrubro(){
    let tipo = this.destinocargolst.filter(item=>item.id == this.regserv.transferenciarubro_reg.idrubrocargo)[0].descripcion
     switch(tipo.toUpperCase()){
      case 'DIEZMOS': this.ctrlserv.saldorubrotransf = this.ctrlserv.cajasaldo.diezmos; break;
      case 'OFRENDAS': this.ctrlserv.saldorubrotransf = this.ctrlserv.cajasaldo.ofrendas; break;
      case 'MISIONES': this.ctrlserv.saldorubrotransf = this.ctrlserv.cajasaldo.misiones; break;
      case 'MISERICORDIA': this.ctrlserv.saldorubrotransf = this.ctrlserv.cajasaldo.misericordia; break;
      case 'PRO-TEMPLO': this.ctrlserv.saldorubrotransf = this.ctrlserv.cajasaldo.protemplo; break;
    }
  }

  guardartransferencia(e: any){
    let request = {
      idrubrocargo: this.regserv.transferenciarubro_reg.idrubrocargo,
      idrubroabona: this.regserv.transferenciarubro_reg.idrubroabona,
      fecha: this.configserv.datepipe.transform(this.regserv.transferenciarubro_reg.fecha, "YYYY-MM-dd") ,
      monto: this.regserv.transferenciarubro_reg.monto,
      idcaja: this.regserv.caja_reg.id,
      rubroabona: this.destinolst.filter(item=>item.id == this.regserv.transferenciarubro_reg.idrubroabona)[0].descripcion,
      rubrocargo: this.destinolst.filter(item=>item.id == this.regserv.transferenciarubro_reg.idrubrocargo)[0].descripcion
    }
     if(request.idrubrocargo.trim() == ''){
      this.valserv.validaban = false;
       this.valserv.showError("Seleccione rubro cargo.", this.titulo)

    }else if(parseFloat(request.monto) <= 0){
      this.valserv.validaban = false;
       this.valserv.showError("Ingrese el monto a transferir.", this.titulo)
    }else if(parseFloat(request.monto) > parseFloat(this.ctrlserv.saldorubrotransf)){
       this.valserv.validaban = false;
       this.valserv.showError("El monto a transferir no puede ser mayor que el saldo del rubro.", this.titulo)
    }else{
      Swal.fire({
        title: this.segserv.namesystem,
        text: "¿Está seguro de realizar la transferencia que se descontará del rubro de " +  request.rubrocargo + " al rubro de " + request.rubroabona + "?",
        icon: 'question',
        showCancelButton: true,
        confirmButtonText: 'Aceptar',
        cancelButtonText: 'Cancelar'
      }).then((result) => {
        if (result.value) {
          this.regserv.registro('Transferencias', 'insert-transferenciarubros', request).then(value=>{
            if(value){
              this.lstserv.listar(this.openList.emit(e))
              switch(request.rubroabona){
                case 'DIEZMOS': this.ctrlserv.saldorubro = parseFloat(this.ctrlserv.saldorubro) +parseFloat(request.monto); break;
                case 'OFRENDAS': this.ctrlserv.saldorubro = parseFloat(this.ctrlserv.saldorubro) +parseFloat(request.monto); break;
                case 'MISIONES': this.ctrlserv.saldorubro = parseFloat(this.ctrlserv.saldorubro) +parseFloat(request.monto); break;
                case 'MISERICORDIA': this.ctrlserv.saldorubro = parseFloat(this.ctrlserv.saldorubro) +parseFloat(request.monto); break;
                case 'PRO-TEMPLO': this.ctrlserv.saldorubro = parseFloat(this.ctrlserv.saldorubro) +parseFloat(request.monto); break;
              }
              this.configserv.onNoClickDialog(this.dialogRef)
            }
          })
        }})

    }
  }

}
