import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TransfrubrosComponent } from './transfrubros.component';

describe('TransfrubrosComponent', () => {
  let component: TransfrubrosComponent;
  let fixture: ComponentFixture<TransfrubrosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TransfrubrosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TransfrubrosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
