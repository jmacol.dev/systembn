import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { Usuario } from 'src/app/models/usuario';
import { ControlesService } from 'src/app/services/controles.service';
import { RegisterService } from 'src/app/services/register.service';
import { SeguridadService } from 'src/app/services/seguridad.service';
import { SesionService } from 'src/app/services/sesion.service';
import { SidenavService } from 'src/app/services/sidenav.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.sass']
})
export class MenuComponent implements OnInit {
  @Output() openNav = new EventEmitter();
  panelOpenState = false;
  constructor(
    public sidenavservc: SidenavService,
    public sserv: SesionService,
    public segurserv: SeguridadService,
    public ctrlserv: ControlesService,
    public regserv: RegisterService
  ) {

  }

  ngOnInit(): void {
    let usu: Usuario = JSON.parse(this.sserv.getuser(sessionStorage.getItem('user')??'{}') ?? '{}')
    this.segurserv.usuarioreg = usu
    this.segurserv.usuarioreg.perfil = usu.perfil == '' ? 'Conectado' : usu.perfil
  }


}
