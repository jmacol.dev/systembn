import { CategoriaCbo } from './../../models/categoria';
import { Component,  EventEmitter,  Input,  OnDestroy,  OnInit, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { NewService } from 'src/app/services/new.service';
import Swal from 'sweetalert2';
import {DateAdapter, MAT_DATE_FORMATS} from '@angular/material/core';
import { AppDateAdapter, APP_DATE_FORMATS } from 'src/app/helpers/format-datepicker';
import { ConfiguracionService } from 'src/app/services/configuracion.service';
import { ListService } from 'src/app/services/list.service';
import { RegisterService } from 'src/app/services/register.service';
import { Caja, CajaCbo } from 'src/app/models/caja';
import { gruporecaudacion } from 'src/app/models/gruporecaudacion';
import { SeguridadService } from 'src/app/services/seguridad.service';
import { Usuario } from 'src/app/models/usuario';
import { ControlesService } from 'src/app/services/controles.service';
import { NavigationEnd, Router } from '@angular/router';


@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.sass'],
  providers: [
    {provide: DateAdapter, useClass: AppDateAdapter},
    {provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS}
  ]

})
export class ButtonComponent implements OnInit, OnDestroy {
  navigationSubscription;
  @Output() openNvo = new EventEmitter();
  @Output() openList = new EventEmitter();
  @Output() openListSC = new EventEmitter();
  @Input() banctrl: any = [false, false, false, false, false, false];
  banfecha: boolean = this.banctrl[0];
  datacbo: CategoriaCbo[] = []
  etapa_lst: CategoriaCbo[] = []
  bancaja: boolean = this.banctrl[1];
  banexportar: boolean = this.banctrl[2];
  bannuevo: boolean = this.banctrl[5];
  banrangfecha: boolean = this.banctrl[3];
  bangrupo: boolean = this.banctrl[4];
  campaignOne: FormGroup = new FormGroup({});
  today: Date | undefined;
  datepast: Date | undefined;
  classanc: String = 'col-lg-5';
  date: any;
  cajamd: string = '3';
  grupocj: string = '0';
  @Input() opcion: number = 0;
  @Input() modulo: string = '';
  public caja_lst: CajaCbo[] = []
  public reporte_lst = [
    { "id": "1", "nombre": "Reporte de ingresos y egresos" },
    { "id": "2", "nombre": "Reporte de ingresos por rubro " },
    { "id": "3", "nombre": "Reporte de egresos por rubro "},
    { "id": "4", "nombre": "Reporte de ingresos por sexo" },
    { "id": "5", "nombre": "Reporte de ingresos por clasificación de edad" },
  ]

  public anio_lst: any = [];
  public mes_lst: any = [];
  public meses = ["ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SETIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE"]
  // public gruporecaudacion_lst: gruporecaudacion[] = []
  // public grecaud_aux: gruporecaudacion[] = []
  public idcaja: string = ''
  panelOpenState = false;
  public anio: number = new Date().getFullYear();
  constructor(
    public nvoservc: NewService,
    public configservc: ConfiguracionService,
    public listserv: ListService,
    public regserv: RegisterService,
    public ctrlserv: ControlesService,
    private router: Router
  ) {
    this.navigationSubscription = this.router.events.subscribe((e: any) => {
      // If it is a NavigationEnd event re-initalise the component
      if (e instanceof NavigationEnd) {
        this.initialiseInvites();
      }
    });

    let date: Date = new Date();

    for (let i = date.getFullYear(); i >= 2020; i--) {
      let item = { "id": i, "nombre": i };
      this.anio_lst.push(item)
    }
    this.anio = date.getFullYear();

    this.seleccionarAnio()
  }
  listadoetapas() {
    // Set default values and re-fetch any data you need.

      this.listserv.listado('list-etapas', '').then(dataaux=>{
        this.datacbo = dataaux;
        this.etapa_lst = this.datacbo.filter(item => item.superior == null)


      })
  }

  seleccionarAnio() {
    this.mes_lst = [];
    let date: Date = new Date();
    if (this.anio == date.getFullYear()) {
      for (let i = 0; i <= date.getMonth(); i++) {
        let item = { "id": i - 1, "nombre": this.meses[i] };
        this.mes_lst.push(item)
      }
    } else {
      for (let i = 0; i <= 11; i++) {
        let item = { "id": i - 1, "nombre": this.meses[i] };
        this.mes_lst.push(item)
      }
    }
  }

  initialiseInvites() {
    // Set default values and re-fetch any data you need.
    if(this.modulo == 'TESORERIA'){
      this.date = new FormControl(new Date());
      this.today = new Date();
      this.datepast = new Date();
      this.datepast.setDate(this.datepast.getDate() - 7);
      this.regserv.movimientocaja_reg.fecha = this.today
      this.campaignOne = new FormGroup({
        start: new FormControl(this.datepast),
        end: new FormControl(this.today)
      });
      this.listserv.fechamax = this.today
      this.listserv.fechamin = this.datepast
      this.regserv.caja_reg = this.regserv.empty_caja()
      this.listarcaja();
      this.ctrlserv.listargruporecaudacion(this.opcion);
      this.ctrlserv.getcajasaldo()

    }
    if(this.modulo == "CONFIGURACION"){
      this.configservc.idcateg = ""
      this.bangrupo = true
      this.bancaja = false
      this.configservc.stcat= true
      this.cambiartipocat(null)
    }

    if(this.modulo == "MEMBRESIA"){
      this.listadoetapas()

    }
  }

  ngOnInit(): void {
    this.initialiseInvites();
    if(this.ctrlserv.isSmallScreen){
      this.ctrlserv.vsald = false
    }
    this.banfecha = this.banctrl[0];
    this.bancaja = this.banctrl[1];
    this.banexportar = this.banctrl[2];
    this.banrangfecha = this.banctrl[3];
    this.bangrupo = this.banctrl[4];
    this.bannuevo = this.banctrl[5];
    switch (this.modulo){
      case 'TESORERIA':
        if ([4,5].indexOf(this.opcion) > -1) {
           this.classanc = 'col-lg-5';
        } else {
          if (!this.banfecha && !this.bancaja && !this.banrangfecha && !this.bangrupo) {
            this.classanc = 'col-12';
          } else {
            this.classanc = 'col-lg-5';
          }
        }
          break;
      case 'CONFIGURACION':
        if (!this.bancaja && !this.bangrupo){
          this.classanc = 'col-12';
        }else{
          this.classanc = 'col-lg-5';
        }
        break;
      case 'MEMBRESIA':
        this.classanc = this.opcion==1 ?'col-lg-5' : 'col-12';
        break;
      default :
          this.classanc = 'col-12';
          break;
    }
  }

  ngOnDestroy() {
    if (this.navigationSubscription) {
      this.navigationSubscription.unsubscribe();
    }
  }

  listEvent(e: any) {
    this.listserv.listar(this.openList.emit(e))
  }

  excelEvent(e: any) {
    this.listserv.excel(this.openList.emit(e))
  }

  pdfEvent(e: any) {
    this.listserv.pdf(this.openList.emit(e))
  }



  listarcaja(){
    this.listserv.listado('list-accesocaja', '').then(data=>{
      this.regserv.caja_reg.id = data[0].id;
      this.caja_lst = data;
      if (this.opcion === 1){
        this.listEvent(null);
      }
    })
  }


  listar_diezmosofrendas($id: string){

  }

  generar_grupo(){
    let fechaInicio = this.regserv.movimientocaja_reg.fecha.getTime();
    let fechaFin    = new Date().getTime();
    var diff = fechaFin - fechaInicio;
    let dias = diff/(1000*60*60*24);
    if(this.ctrlserv.grecaud_aux.length > 0 && parseInt(dias.toString()) > this.ctrlserv.diasmax){
      Swal.fire("Grupo de recaudación", "No puede generar un grupo de recaudación con una fecha anterior. El máximo permitido es de " + this.ctrlserv.diasmax + " día(s)", "info");
    }else{
      if(this.regserv.caja_reg.id == 'TODOS'){
        Swal.fire("Grupo de recaudación", "Debe seleccionar una caja para generar un grupo de recaudación", "info");
      }else{
        Swal.fire({
          title: "Grupo de recaudación",
          text: '¿Desea generar un grupo de recaudación para la caja seleccionada con la fecha de hoy?',
          icon: 'question',
          showCancelButton: true,
          confirmButtonText: 'Aceptar',
          cancelButtonText: 'Cancelar'
        }).then((result) => {
          let caja_aux = this.caja_lst.filter(item => item.id == this.regserv.caja_reg.id)[0]
          if (result.value) {
              this.regserv.registro('Grupo de recaudación', 'insert-gruporecaudacion', {
                codigo: caja_aux.codigo,
                idcaja: this.regserv.caja_reg.id,
                fecha: this.configservc.datepipe.transform(this.regserv.movimientocaja_reg.fecha, "YYYY-MM-dd")
              }).then(value=>{
                if(value){
                  this.ctrlserv.seleccionarcaja();
                  this.ctrlserv.listargruporecaudacion(this.opcion);

                }
              })
        }})
      }
    }
  }

  newEvent(e: any, $egreso:boolean = true) {
    this.nvoservc._egreso = $egreso;
    switch (this.modulo){
      case 'TESORERIA':

        if (this.opcion === 1){ //MÓDULO DE MOVIMIENTOS DE CAJA
          if (this.regserv.caja_reg.id === 'TODOS'){
            Swal.fire('Tesorería', 'Seleccione una caja para registrar la salida de dinero.', 'info');
          }else {this.nvoservc.nuevo(this.openNvo.emit(e)); }
        }

        if (this.opcion === 2){ //MÓDULO DE DIEZMOS y OFRENDAS
          if (this.regserv.caja_reg.id === 'TODOS'){
            Swal.fire('Tesorería', 'Seleccione una caja para registrar diezmos y ofrendas.', 'info');
          }else if (this.regserv.gruporecaudacion_reg.id === '0' || this.regserv.gruporecaudacion_reg.id === '' || this.regserv.gruporecaudacion_reg.id === 'TODOS'){
            Swal.fire('Tesorería', 'Seleccione grupo de recaudación para registrar diezmos y ofrendas.', 'info');
          }else if(this.regserv.gruporecaudacion_reg.fechacierre != null){
            Swal.fire('Tesorería', 'Grupo de recaudación está cerrado. Si desea agregar diezmos y ofrendas genere un nuevo grupo de recaudación.', 'info');
          } else{ this.nvoservc.nuevo(this.openNvo.emit(e)); }
        }

        if (this.opcion === 3){ //MÓDULO DE CAJA Y ACCESOS
          this.nvoservc.nuevo(this.openNvo.emit(e));
        }
        if (this.opcion === 4){ //MÓDULO DE CAJA Y ACCESOS
          this.nvoservc.nuevo(this.openNvo.emit(e));
        }
        break;
      case 'CONFIGURACION':

        if (this.opcion === 1) { //MÓDULO DE CATEGORÍAS
          this.nvoservc.nuevo(this.openNvo.emit(e));
        }
        break;
      case 'SEGURIDAD':

        if (this.opcion === 1) { //MÓDULO DE CATEGORÍAS
          this.nvoservc.nuevo(this.openNvo.emit(e));
        }
        break;
      case 'MEMBRESIA':
         this.nvoservc.nuevo(this.openNvo.emit(e));
        break;
      default :
        break;
    }
  }

  cambiartipocat(e: any){
    if (!this.configservc.stcat){
      this.configservc.stcat = true;
      this.configservc.iconcat = 'arrow-circle-left';
      this.configservc.textcat = 'Regresar a categorías';
      this.configservc.btncat = 'danger';
      this.configservc.tipocat = 'subcategorías';
    }else{
      this.configservc.stcat = false;
      this.configservc.iconcat = 'arrow-circle-right';
      this.configservc.textcat = 'Ir a subcategorías';
      this.configservc.btncat = 'dark';
      this.configservc.tipocat = 'categorías';
    }
    this.configservc.seleccioncategoria();
    this.listserv.listar(this.openList.emit(e))
  }


}
