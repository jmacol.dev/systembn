import { InformeMensualComponent } from './components/tesoreria/informe-mensual/informe-mensual.component';
import { EstadisticasComponent } from './components/tesoreria/estadisticas/estadisticas.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EgresosComponent } from './components/tesoreria/egresos/egresos.component';
import { IngresosComponent } from './components/tesoreria/ingresos/ingresos.component';
import { MiembrosComponent } from './components/membresia/miembros/miembros.component';
import { AppComponent } from './app.component';
import { CajaComponent } from './components/tesoreria/caja/caja.component';
import { DashboardComponent } from './components/home/dashboard/dashboard.component';
import { CategoriasComponent } from './components/configuracion/categorias/categorias.component';
import { LoginComponent } from './components/home/login/login.component';
import { AccesoscajaComponent } from './components/tesoreria/accesoscaja/accesoscaja.component';
import { ButtonComponent } from './template/button/button.component';
import { FamiliasComponent } from './components/membresia/familias/familias.component';
import { UsuariosComponent } from './components/seguridad/usuarios/usuarios.component';
import { ActivoGuard } from './guards/activo.guard';

const routes: Routes = [
  { path: 'login', component: LoginComponent },

  { path: 'diezmosofrendas', component: IngresosComponent, runGuardsAndResolvers: 'always' },
  { path: 'caja', component: CajaComponent, runGuardsAndResolvers: 'always', canActivate:[ActivoGuard] },
  { path: 'dashboard', component: DashboardComponent, runGuardsAndResolvers: 'always', canActivate:[ActivoGuard]  },
  { path: 'categorias', component: CategoriasComponent, runGuardsAndResolvers: 'always', canActivate:[ActivoGuard]   },
  { path: 'accesoscaja', component: AccesoscajaComponent, runGuardsAndResolvers: 'always', canActivate:[ActivoGuard]   },
  { path: 'estadisticas', component: EstadisticasComponent, runGuardsAndResolvers: 'always', canActivate:[ActivoGuard]   },
  { path: 'informe-mensual', component: InformeMensualComponent, runGuardsAndResolvers: 'always', canActivate:[ActivoGuard]  },
  { path: 'personas', component: MiembrosComponent, runGuardsAndResolvers: 'always', canActivate:[ActivoGuard]   },
  { path: 'familias', component: FamiliasComponent, runGuardsAndResolvers: 'always', canActivate:[ActivoGuard]   },
  { path: 'usuarios', component: UsuariosComponent, runGuardsAndResolvers: 'always', canActivate:[ActivoGuard]  },
   { path: '', redirectTo: 'login', pathMatch:'full' },
  { path: '**', redirectTo: 'login'},

];

@NgModule({
  imports: [RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload', relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
